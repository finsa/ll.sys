  <nav class="main-header navbar navbar-expand text-sm navbar-dark navbar-success">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <!--li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li-->
    </ul>

    
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                <span class="d-none d-md-inline"><?php echo $this->session->first_name . ' ' . $this->session->last_name ?></span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <li class="user-header bg-success" style="height:75px">
                    <p><?php echo $this->session->first_name . ' ' . $this->session->last_name ?>
                        <small><?php echo $this->session->group ?></small>
                    </p>
                </li>
                <li class="user-footer">
                    <a href="<?php echo site_url('profile') ?>" class="btn btn-primary btn-flat">Profile</a>
                    <a href="<?php echo site_url('logout') ?>" class="btn btn-flat btn-danger float-right">Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</nav>