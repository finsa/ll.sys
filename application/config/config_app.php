<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['api_prefix']       = 'CMS';
$config['api_suffix']       = '@&#';
$config['api_expire']       = 480;                      // 480 menit
$config['api_user_id']      = "API-User";
$config['api_token_name']   = "API-Token";
$config['api_identifier']   = "API-Identifier";
$config['use_cache']        = true;                     // cache output html
$config['cache_time']       = 480;                      // lama waktu simpan cache. dalam menit
$config['use_minify']       = false;                     // Minify output html, css, js
$config['minify_level']     = 1;                        // 1: simple, 2: one line (based on https://github.com/zawaruddin/codeigniter-minifier)
$config['use_cdns']         = false;                    // penggunaan cdns
$config['page_title']       = "LL CMS";
$config['app_name']         = "LL CMS";
$config['app_alias']        = "LL CMS";
$config['footer_left']      = "LL CMS";
$config['footer_right']     = "LL CMS";
$config['cdn_url']          = "http://localhost/cms.cdn";
$config['img_dir']          = "images/";
$config['upload_path']      = "./../cms.cdn/images/"; 
$config['allowed_types']    = "jpg|png|jpeg"; 
$config['encrypt_name']     = true; 
$config['max_size']         = 2048;  