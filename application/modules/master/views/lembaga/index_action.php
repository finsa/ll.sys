<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="lembaga-form" width="80%">
<div id="modal-lembaga" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="txt_nama_lembaga" class="col-sm-3 col-form-label">Nama Lembaga <i class="required">*</i></label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm get_slug" id="txt_nama_lembaga" placeholder="Nama Lembaga" name="txt_nama_lembaga" value="<?=isset($data->txt_nama_lembaga)? $data->txt_nama_lembaga : ''?>" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="txt_deskripsi" placeholder="Deskripsi Lengkap" name="txt_deskripsi" value="<?=isset($data->txt_deskripsi)? $data->txt_deskripsi : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="txt_url" class="col-sm-3 col-form-label">Facebook URL</i></label>
				<div class="col-sm-9">
					<input type="text" class="form-control form-control-sm" id="txt_url" placeholder="Facebook URL" name="txt_url" value="<?=isset($data->txt_url)? $data->txt_url : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label class="col-sm-3"> </label>
				<div class="col-sm-9 mt-2">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="int_status" value="1" <?=isset($data->int_status)? (($data->int_status == 1)? 'checked' : '') : 'checked'?>>
							<label for="radioPrimary1">OPD </label>
					</div>
					<div class="icheck-danger d-inline is_aktif">
						<input type="radio" id="radioPrimary2" name="int_status" value="0" <?=isset($data->int_status)? (($data->int_status == 0)? 'checked' : '') : '' ?>>
						<label for="radioPrimary2">Non-OPD</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$("#lembaga-form").validate({
			rules: {
			    txt_nama_lembaga:{
			        required: true,
					minlength: 2,
					maxlength: 50
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-lembaga';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#lembaga-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>