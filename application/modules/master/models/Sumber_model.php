<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Sumber_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_id_sumber, txt_sumber, txt_prefix
								 FROM	{$this->m_sumber}
								 ORDER BY int_id_sumber ASC")->result();
	}

	public function get_list_user(){
		$int_id_user = $this->session->userdata['user_id'];
		return $this->db->query("SELECT ms.int_id_sumber, ms.txt_sumber, ms.txt_prefix
								FROM m_user_admin_sumber uas
								LEFT JOIN m_sumber ms ON uas.`int_id_sumber` = ms.`int_id_sumber`
								WHERE uas.int_id_user = $int_id_user")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_sumber);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_sumber', $filter)
					->group_end();
		}

		$order = 'txt_sumber ';
		switch($order_by){
			case 1 : $order = 'txt_sumber '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_sumber);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('txt_sumber', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		return $this->db->insert($this->m_sumber, $ins);
	}

	public function get($int_id_sumber){
		return $this->db->select("*")
					->get_where($this->m_sumber, ['int_id_sumber' => $int_id_sumber])->row();
	}

	public function update($int_id_sumber, $ins){
		$this->db->trans_begin();

		$this->db->where('int_id_sumber', $int_id_sumber);
		$this->db->update($this->m_sumber, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_id_sumber){
		$this->db->trans_begin();
		$this->db->delete($this->m_sumber,  ['int_id_sumber' => $int_id_sumber]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
