<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Kategori_model extends MY_Model {

	public function get_list(){
		return $this->db->query("SELECT int_id_kategori, txt_kategori 
								 FROM	{$this->m_kategori}
								 ORDER BY int_id_kategori ASC")->result();
	}

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
					->from($this->m_kategori);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_kategori', $filter)
					->group_end();
		}

		$order = 'txt_kategori ';
		switch($order_by){
			case 1 : $order = 'txt_kategori '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_kategori);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('txt_kategori', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		return $this->db->insert($this->m_kategori, $ins);
	}

	public function get($int_id_kategori){
		return $this->db->select("*")
					->get_where($this->m_kategori, ['int_id_kategori' => $int_id_kategori])->row();
	}

	public function update($int_id_kategori, $ins){
		$this->db->trans_begin();

		$this->db->where('int_id_kategori', $int_id_kategori);
		$this->db->update($this->m_kategori, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_id_kategori){
		$this->db->trans_begin();
		$this->db->delete($this->m_kategori,  ['int_id_kategori' => $int_id_kategori]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
