<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lembaga_model extends MY_Model {


	public function get_list(){
		return $this->db->query("SELECT int_id_lembaga, CONCAT(txt_nama_lembaga, ' - ', txt_deskripsi) as txt_lembaga 
								 FROM	{$this->m_lembaga}
								 ORDER BY int_id_lembaga ASC")->result();
	}

	public function get_list_user(){
		$int_id_user = $this->session->userdata['user_id'];
		return $this->db->query("SELECT ml.int_id_lembaga, CONCAT(ml.txt_nama_lembaga, ' - ', ml.txt_deskripsi) as txt_lembaga
								FROM m_user_admin_lembaga ual
								LEFT JOIN m_lembaga ml ON ual.`int_id_lembaga` = ml.`int_id_lembaga`
								WHERE ual.int_id_user = $int_id_user")->result();
	}

	public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("CONCAT(txt_nama_lembaga, ' - ', txt_deskripsi) as txt_lembaga, txt_url, int_id_lembaga")
					->from($this->m_lembaga);

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
				->like('txt_nama_lembaga', $filter)
				->or_like('txt_deskripsi', $filter)
					->group_end();
		}

		$order = 'int_order ';
		switch($order_by){
			case 1 : $order = 'txt_lembaga '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->m_lembaga);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('txt_nama_lembaga', $filter)
			->or_like('txt_deskripsi', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins){
		return $this->db->insert($this->m_lembaga, $ins);
	}

	public function get($int_id_lembaga){
		return $this->db->select("*")
					->get_where($this->m_lembaga, ['int_id_lembaga' => $int_id_lembaga])->row();
	}

	public function update($int_id_lembaga, $ins){
		$this->db->trans_begin();

		$this->db->where('int_id_lembaga', $int_id_lembaga);
		$this->db->update($this->m_lembaga, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_id_lembaga){
		$this->db->trans_begin();
		$this->db->delete($this->m_lembaga,  ['int_id_lembaga' => $int_id_lembaga]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}
