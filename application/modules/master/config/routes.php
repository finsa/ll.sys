<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['kategori']['get']          			    = 'master/kategori';
$route['kategori']['post']         			    = 'master/kategori/list';
$route['kategori/add']['get']      			    = 'master/kategori/add';
$route['kategori/save']['post']    			    = 'master/kategori/save';
$route['kategori/([a-zA-Z0-9]+)']['get']        = 'master/kategori/get/$1';
$route['kategori/([a-zA-Z0-9]+)']['post']     	= 'master/kategori/update/$1';
$route['kategori/([a-zA-Z0-9]+)/del']['get']    = 'master/kategori/confirm/$1';
$route['kategori/([a-zA-Z0-9]+)/del']['post'] 	= 'master/kategori/delete/$1';

$route['lembaga']['get']          				= 'master/lembaga';
$route['lembaga']['post']         				= 'master/lembaga/list';
$route['lembaga/add']['get']      				= 'master/lembaga/add';
$route['lembaga/save']['post']    				= 'master/lembaga/save';
$route['lembaga/([a-zA-Z0-9]+)']['get']     	= 'master/lembaga/get/$1';
$route['lembaga/([a-zA-Z0-9]+)']['post']    	= 'master/lembaga/update/$1';
$route['lembaga/([a-zA-Z0-9]+)/del']['get'] 	= 'master/lembaga/confirm/$1';
$route['lembaga/([a-zA-Z0-9]+)/del']['post']	= 'master/lembaga/delete/$1';

$route['sumber']['get']                         = 'master/sumber';
$route['sumber']['post']                        = 'master/sumber/list';
$route['sumber/add']['get']                     = 'master/sumber/add';
$route['sumber/save']['post']                   = 'master/sumber/save';
$route['sumber/([a-zA-Z0-9]+)']['get']     	    = 'master/sumber/get/$1';
$route['sumber/([a-zA-Z0-9]+)']['post']    	    = 'master/sumber/update/$1';
$route['sumber/([a-zA-Z0-9]+)/del']['get'] 	    = 'master/sumber/confirm/$1';
$route['sumber/([a-zA-Z0-9]+)/del']['post']	    = 'master/sumber/delete/$1';
