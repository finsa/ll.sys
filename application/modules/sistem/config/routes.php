<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['facebook']['get']          				= 'sistem/facebook';
$route['facebook']['post']    				    = 'sistem/facebook/save';
//$route['facebook/feed']['get']                  = 'sistem/facebook/getGroupFeed';

$route['grab']['get']                           = 'sistem/grab';

$route['s_user']['get']          				= 'sistem/user';
$route['s_user']['post']         				= 'sistem/user/list';
$route['s_user/add']['get']      				= 'sistem/user/add';
$route['s_user/save']['post']    				= 'sistem/user/save';
$route['s_user/([a-zA-Z0-9]+)']['get']     		= 'sistem/user/get/$1';
$route['s_user/([a-zA-Z0-9]+)']['post']    		= 'sistem/user/update/$1';
$route['s_user/([a-zA-Z0-9]+)/del']['get'] 		= 'sistem/user/confirm/$1';
$route['s_user/([a-zA-Z0-9]+)/del']['post']		= 'sistem/user/delete/$1';

$route['s_menu']['get']          				= 'sistem/menu';
$route['s_menu']['post']         				= 'sistem/menu/list';
$route['s_menu/add']['get']      				= 'sistem/menu/add';
$route['s_menu/save']['post']    				= 'sistem/menu/save';
$route['s_menu/([a-zA-Z0-9]+)']['get']     		= 'sistem/menu/get/$1';
$route['s_menu/([a-zA-Z0-9]+)']['post']    		= 'sistem/menu/update/$1';
$route['s_menu/([a-zA-Z0-9]+)/del']['get'] 		= 'sistem/menu/confirm/$1';
$route['s_menu/([a-zA-Z0-9]+)/del']['post']		= 'sistem/menu/delete/$1';

$route['s_group']['get']          				= 'sistem/group';
$route['s_group']['post']         				= 'sistem/group/list';
$route['s_group/add']['get']      				= 'sistem/group/add';
$route['s_group/save']['post']    				= 'sistem/group/save';
$route['s_group/([a-zA-Z0-9]+)']['get']     	= 'sistem/group/get/$1';
$route['s_group/([a-zA-Z0-9]+)']['post']    	= 'sistem/group/update/$1';
$route['s_group/([a-zA-Z0-9]+)/del']['get'] 	= 'sistem/group/confirm/$1';
$route['s_group/([a-zA-Z0-9]+)/del']['post']	= 'sistem/group/delete/$1';
$route['s_group/([a-zA-Z0-9]+)/menu']['get']    = 'sistem/group/menu_get/$1';
$route['s_group/([a-zA-Z0-9]+)/menu']['post']   = 'sistem/group/menu_save/$1';