<div class="container-fluid">
	<div class="row">
		<section class="col-lg-12">
			<div class="card card-outline">
				<div class="card-header">
					<h3 class="card-title mt-1">
						<i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
						<?=$breadcrumb->title?>
					</h3>
					<div class="card-tools">
						<button type="button" data-block="body" class="btn btn-xs btn-primary ajax_modal" data-url="<?=$url ?>" ><i class="fas fa-plus"></i> Add</button>
					</div>
				</div><!-- /.card-header -->
				<div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group row text-sm mb-0">
                                    <label for="level_filter" class="col-md-3 col-form-label">Level</label>
                                    <div class="col-md-9">
                                        <select id="level_filter" name="level_filter" class="form-control form-control-sm level_filter" style="width: 100%;">
                                            <option value="">- All Level -</option>
                                            <?php
                                            foreach($level as $g){
                                                echo '<option value="'.$g->level.'">'.$g->level.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group row text-sm mb-0">
                                    <label for="parent_filter" class="col-md-3 col-form-label">Parent</label>
                                    <div class="col-md-9">
                                        <select id="parent_filter" name="parent_filter" class="form-control form-control-sm parent_filter" style="width: 100%;">
                                            <option value="">- All Parent -</option>
                                            <?php
                                            foreach($parent as $g){
                                                echo '<option value="'.$g->menu_id.'">'.$g->level.' - '.$g->nama.'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<table class="table table-striped table-hover table-full-width" id="table_menu">
						<thead>
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Nama</th>
								<th>Url</th>
								<th>Class</th>
								<th>Lvl</th>
								<th>Urut</th>
								<th>Parent</th>
								<th>Stat</th>
								<th></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</section>
	</div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
