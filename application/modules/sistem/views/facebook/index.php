<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <div class="tab-content">
                        <?php
                            //print_r($facebook);
                            echo form_open($url_update, ['method' => "POST", "role" => "form", "class" => "form-horizontal", "id" =>"facebook-form"]);
                            $msg = $this->session->flashdata('msg');
                            if(!empty($msg)){
                                echo '<div class="alert alert-'.(($this->session->flashdata('stat'))? 'success' : 'danger').'">'.$msg.'</div>';
                            }
                        ?>
                            <div class="form-group row mb-1">
                                <label for="group" class="col-sm-3 col-form-label">Group ID</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="var_id_group" id="var_id_group" value="<?php echo $facebook->var_id_group?>"/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">App ID</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="txt_app_id" id="txt_app_id" value="<?php echo $facebook->txt_app_id?>"/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">App Secret</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="txt_app_secret" id="txt_app_secret" value="<?php echo $facebook->txt_app_secret?>"/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Exchange Token</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="txt_exc_token" id="txt_exc_token" value=""/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Grab Limit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="int_grab_limit" id="int_grab_limit" value="<?php echo $facebook->int_grab_limit?>"/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Long Lived Token</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="txt_long_token" id="txt_long_token" value="<?php echo $facebook->txt_long_token?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Last Grab</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="dt_last_grab" id="dt_last_grab" value="<?php echo $facebook->dt_last_grab?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Token Input Date</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="dt_token_input" id="dt_token_input" value="<?php echo $facebook->dt_token_input?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Token Expired Date</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" name="dt_token_expired" id="dt_token_expired" value="<?php echo $facebook->dt_token_expired?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>