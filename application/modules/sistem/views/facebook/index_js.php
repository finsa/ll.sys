<script>
    $(document).ready(function(){
        $("#facebook-form").validate({
            rules: {
                var_id_group: {
                    required: true
                },
                txt_exc_token: {
                    required: true
                },
                txt_app_id: {
                    required: true
                },
                txt_app_secret: {
                    required: true
                }
            },
            validClass: "valid-feedback",
            errorElement: "div", // contain the error msg in a small tag
            errorClass: 'invalid-feedback',
            errorPlacement: erp,
            highlight: hl,
            unhighlight: uhl,
            success: sc
        });

    });
</script>