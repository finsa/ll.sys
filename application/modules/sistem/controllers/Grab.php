<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grab extends MX_Controller {
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'GRAB';
        $this->module   = 'sistem';
        $this->routeURL = 'grab';

		$this->load->model('facebook_model', 'model');
		$this->load->model('pelaporan/lapor_model', 'lapor');
        $this->config->set_item('compress_output', FALSE);
    }

    public function index(){
        $this->config->set_item('item_name', 'item_value');
         //$config['compress_output'] = FALSE;
        $grab = $this->model->get_data();
        //print_r($grab);
        $var_id_group = $grab->var_id_group;
        $txt_long_token = $grab->txt_long_token;
        $int_grab_limit = $grab->int_grab_limit;
        $dt_last_grab = $grab->dt_last_grab;
        $url = "https://graph.facebook.com/".$var_id_group."/feed?fields=message,id,created_time,permalink_url,attachments{media{image{src}}}&limit=".$int_grab_limit."&access_token=".$txt_long_token."";

        $now = date('Y-m-d H:i:s');
        $diff = date_diff(date_create($dt_last_grab),date_create($now));
        $hours = $diff->format("%h");
        $minutes = $diff->format("%i");
        $second = $diff->format("%s") + ($hours * 360) + ($minutes * 60);
        //echopre ($second); die;

        $stat = false;

        if($second > 300){
            $insert = 0;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);


            curl_close($curl);
            $arrRespone = (json_decode($response, true));
            //print_r($response);die;

            if(array_key_exists("error",$arrRespone)) {
                //echo $arrRespone['error']['message'];
                $stat = false;
                $msg = 'Error Facebook API';
            }else{
                $data = $arrRespone['data'];
                $now = date("Y-m-d H:i:s");
                $row = array();
                
                foreach($data as $row){
                    if(array_key_exists("message",$row)){
                        $lampiran_insert  = [];
                        $dtCreated = str_replace("T"," ",$row["created_time"]);
                        $dtCreateds = date("Y-m-d H:i:s", strtotime($dtCreated));
                        $ins['dt_tanggal_pelaporan'] = date($dtCreateds, strtotime("+7 hours"));
                        $ins['dt_created'] = date($dtCreateds, strtotime("+7 hours"));
                        $ins['txt_detil_pelaporan'] = $row["message"];
                        $ins['dt_updated'] = $now;
                        $ins['txt_url'] = $row["permalink_url"];
                        $var_kode_pelaporan = str_replace("463531117794163_","",$row["id"]);
                        $ins['var_kode_pelaporan'] = "FB".$var_kode_pelaporan;
                        $ins['int_id_sumber'] = '1';
                        $ins['created_by'] = '0';                        
                        $ins['int_status'] = '9';
                        $ins['is_active'] = '1';
                        $ins['int_id_lembaga'] = [];
                        if(array_key_exists("attachments",$row)){
                            $lampiran_insert 	= array([
                                                    'txt_desc' => 'FB',
                                                    'is_cover' => 1,
                                                    'dir' => '',
                                                    'int_source' => 1,
                                                    'file_name' => $row['attachments']['data'][0]['media']['image']['src'],
                                                    'file_size' => '',
                                                    'file_type' => 'url']);
                        }
                        $check = $this->model->check_fb($var_kode_pelaporan);
                        $stat = false;
                        if($check==0){
                            //echo $check."-"; 
                            //$db = $this->pelaporan_model->insert_pelaporan($ins);
                            //print_r($ins);
                            //print_r($lampiran_insert);
                            $stat = $this->lapor->create($ins, $lampiran_insert);
                            $insert++;
                        }
                    }
                }
                $upd['dt_last_grab'] = $now;
                $update = $this->model->update($upd);
                $msg = 'Berhasil memuat '.$insert.' data pelaporan';
            }
        }else{
            $msg = 'Data sudah up-to-date, coba beberapa saat lagi';
        }
        $this->set_json(array('stat' => $stat, 
                            'msg' => $msg,
                            $this->getCsrfName() => $this->getCsrfToken()));
    }

//GET LONG LIVE ACCESS TOKEN
/*"https://graph.facebook.com/{graph-api-version}/oauth/access_token?  
    grant_type=fb_exchange_token&          
    client_id={app-id}&
    client_secret={app-secret}&
    fb_exchange_token={your-access-token}"
*/
}
