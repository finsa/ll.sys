<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook extends MX_Controller {
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'FACEBOOK';
        $this->module   = 'sistem';
        $this->routeURL = 'facebook';
		$this->authCheck();

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;

		$this->load->model('facebook_model', 'model');
		$this->load->model('pelaporan/lapor_model', 'lapor');
    }
	
	public function index(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $this->breadcrumb->title = 'Facebook API';
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        $data['facebook']        = $this->model->get_data();
        $data['url_update']     = site_url("{$this->routeURL}");
        //$data['url_pass']       = site_url("{$this->routeURL}/pass");
		$this->render_view('facebook/index', $data, false);
	}

    public function save(){
        $this->authCheckDetailAccess('u'); // hak akses untuk render page

        $this->form_validation->set_rules('var_id_group', 'Group ID', 'required');
        $this->form_validation->set_rules('txt_exc_token', 'Exchange Token', 'required');
        $this->form_validation->set_rules('txt_app_id', 'Apps ID', 'required');
        $this->form_validation->set_rules('txt_app_secret', 'Apps Secret', 'required');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('stat', false);
            $this->session->set_flashdata('msg', 'Data Facebook API Gagal Disimpan. '.validation_errors('<br>', ' '));
        }else{
            $data = $this->input_post();
            $client_id = $data['txt_app_id'];
            $client_secret = $data['txt_app_secret'];
            $fb_exchange_token = $data['txt_exc_token'];

            $url = "https://graph.facebook.com/v6.0/oauth/access_token?grant_type=fb_exchange_token&client_id=".$client_id."&client_secret=".$client_secret."&fb_exchange_token=".$fb_exchange_token."";

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);


            curl_close($curl);
            $arrRespone = (json_decode($response, true));

            if(array_key_exists("error",$arrRespone)) {
                $this->session->set_flashdata('msg', 'Data Facebook API Gagal Disimpan. '.validation_errors('<br>', ' '));
            }else{
                $now = date("Y-m-d H:i:s");
                $data['txt_long_token'] = $arrRespone['access_token'];
                $data['dt_token_input'] = $now;
                $expires_in = $arrRespone['expires_in'];
                $data['dt_token_expired'] = date('Y-m-d H:i:s',strtotime('+'.$expires_in.' seconds',strtotime($now)));

                $this->model->update($data);
                //echo $insert;
                $this->session->set_flashdata('stat', true);
                $this->session->set_flashdata('msg', 'Data Facebook API Berhasil disimpan.');
                }

        }
        redirect('facebook');
    }

    public function getPost(){
        if(!$this->input->is_ajax_request()){
            echo "Ilegal!!!";die;
        }
		$post = $this->input->post();
		$urlFB = $post['txtUrl'];

        $grab = $this->pelaporan_model->getGrab();
        $fbgId = $grab[0]['idGroup'];
        $token = $grab[0]['tokenGroup'];
        $limit = $grab[0]['intLimit'];
        $last = $grab[0]['dtGrab'];
        
		$subUrl = substr($urlFB, strrpos($urlFB, 'permalink/'));
		$postId = str_replace("permalink/","",$subUrl);
		$postId = str_replace("/","",$postId);

        $check = $this->pelaporan_model->check_pelaporan($postId);
        $retVal = array();
        if($check==1){
            $varKodePelaporan = "FB".$postId;
            $retval['status'] = false;
            $retval['data']['url'] = base_url().'pelaporan/ubah/'.$varKodePelaporan.'';
        }else{
            $url = "https://graph.facebook.com/".$fbgId."_".$postId."?access_token=".$token."";

            $curl = curl_init();
    
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                ),
            ));
    
            $response = curl_exec($curl);
            $err = curl_error($curl);
    
    
            curl_close($curl);
            $resDecode =  (json_decode($response, true));
    
            $retval['status'] = true;
            $created_time = substr(str_replace("T"," ",$resDecode['created_time']),0,16);
            $retval['data'][]['created_time'] = $created_time;
            $retval['data'][]['message'] = $resDecode['message'];
        }
        echo json_encode($retval);
    }

    function get_long_token($data){
        $client_id = $data['txt_app_id'];
        $client_secret = $data['txt_app_secret'];
        $fb_exchange_token = $data['txt_exc_token'];

        $url = "https://graph.facebook.com/v6.0/oauth/access_token?grant_type=fb_exchange_token&client_id=".$client_id."&client_secret=".$client_secret."&fb_exchange_token=".$fb_exchange_token."";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);


        curl_close($curl);
            $arrRespone = (json_decode($response, true));

            if(array_key_exists("error",$arrRespone)) {
                return false;
            }else{
                $now = date("Y-m-d H:i:s");
                $data['txt_long_token'] = $arrRespone['txt_long_token'];
                $data['dt_token_input'] = $now;
                $expires_in = $arrRespone['expires_in'];
                $data['dt_token_expired'] = date('Y-m-d H:i:s',strtotime('+'.$expires_in.' seconds',strtotime($now)));


                $update = $this->pelaporan_model->updateGrab($data);
                
                echo $insert;
            }
    }

//GET LONG LIVE ACCESS TOKEN
/*"https://graph.facebook.com/{graph-api-version}/oauth/access_token?  
    grant_type=fb_exchange_token&          
    client_id={app-id}&
    client_secret={app-secret}&
    fb_exchange_token={your-access-token}"
*/
}
