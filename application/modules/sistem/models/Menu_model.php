<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Menu_model extends MY_Model {

	public function list($level = null, $parent = null, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("	a.int_id_menu as menu_id, a.txt_kode as kode, a.txt_nama as nama, a.txt_url as url, a.int_level as level, 
		                    a.int_urutan as urutan, b.txt_nama as parent_name, a.txt_class as class, a.txt_icon as icon, a.is_active as is_aktif,
							CASE WHEN a.int_parent_id IS NULL THEN '' ELSE a.int_parent_id END as parent ")
					->from($this->s_menu. ' a ')
					->join($this->s_menu. ' b ', 'a.int_parent_id = b.int_id_menu ', 'left');

        if(!empty($level) && ctype_digit($level)){
            $this->db->where('a.int_level', $level);
        }

        if(!empty($parent)){
            $this->db->where('a.int_parent_id', $parent);
        }

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('a.txt_nama', $filter)
					->or_like('a.txt_url', $filter)
					->group_end();
		}

		$order = 'a.txturutan ';
		switch($order_by){
			case 1 : $order = 'a.txt_kode '; break;
			case 2 : $order = 'a.txt_nama '; break;
			case 3 : $order = 'a.txt_url '; break;
			case 4 : $order = 'a.int_urutan '; break;
			case 5 : $order = 'b.txt_nama '; break;
			case 6 : $order = 'a.is_active '; break;
			default : $order = 'a.int_urutan '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($level = null, $parent = null, $filter = NULL){
		$this->db->from($this->s_menu. ' a')
					->join($this->s_menu. ' b', 'a.int_parent_id = b.int_id_menu', 'left');

		if(!empty($level) && ctype_digit($level)){
		    $this->db->where('a.int_level', $level);
        }

        if(!empty($parent)){
            $this->db->where('a.int_parent_id', $parent);
        }

		if(!empty($filter)){ // filters 
	        $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('a.txt_nama', $filter)
					->or_like('a.txt_url', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function create($in){
        $col['txt_kode']		= strtoupper($in['kode']);
        $col['txt_nama']		= $in['nama'];
        $col['txt_url']		= empty($in['url'])? NULL : $in['url'];
        $col['int_level']	= $in['level'];
        $col['int_urutan']	= $in['urutan'];
        $col['int_parent_id']	= empty($in['parent'])? NULL : $in['parent'];
        $col['txt_class']	= $in['class'];
        $col['txt_icon']		= $in['icon'];
        $col['is_active']	= $in['is_aktif'];

		$this->db->insert($this->s_menu, $col);
	}

	public function getLevelMenu(){
        return $this->db->query("SELECT DISTINCT int_level as level FROM	{$this->s_menu} ORDER BY int_level ASC")->result();
    }

    public function getParentMenu(){
        return $this->db->query("SELECT int_id_menu as menu_id, txt_nama as nama, txt_kode kode, int_level level FROM {$this->s_menu} WHERE txt_url IS NULL ORDER BY int_level ASC, int_urutan ASC")->result();
    }

	public function get($menu_id){
		return $this->db->query("	SELECT 	a.int_id_menu as menu_id, a.txt_kode as kode, a.txt_nama as nama, a.txt_url as url, a.int_urutan as urutan, a.int_level as level, a.txt_class as class, a.txt_icon as icon, a.is_active as is_aktif, a.int_parent_id as parent
									FROM	{$this->s_menu} a  
									WHERE	a.int_id_menu = ?", [$menu_id])->row();
	}

	public function update($menu_id, $in){
        $col['txt_nama']		= $in['nama'];
        $col['txt_url']		= empty($in['url'])? NULL : $in['url'];
        $col['int_level']	= $in['level'];
        $col['int_urutan']	= $in['urutan'];
        $col['int_parent_id']	= empty($in['parent'])? NULL : $in['parent'];
        $col['txt_class']	= $in['class'];
        $col['txt_icon']		= $in['icon'];
        $col['is_active']	= $in['is_aktif'];

		$this->db->trans_begin();
		$this->db->where('int_id_menu', $menu_id);
		$this->db->update($this->s_menu, $col);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($menu_id){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_menu} WHERE int_id_menu = ?", [$menu_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function getMapMenu($group_id, $tag = '', $parent_id = null){
		$this->db->select("m.int_id_menu as menu_id, m.txt_url as url, m.txt_nama as nama, m.int_level as level, m.int_urutan as urutan, (SELECT COUNT(*) FROM {$this->s_menu} mm WHERE mm.int_parent_id = m.int_id_menu) as sub, gm.c, gm.r, gm.u, gm.d")
							->from($this->s_menu.' m')
							->join($this->s_group_menu.' gm', "m.int_id_menu = gm.int_id_menu AND gm.int_id_group = {$this->db->escape($group_id)}", 'left', false)
							->where('m.is_active', 1)
							->order_by('m.int_urutan');
		if(empty($parent_id)){
			$this->db->where("(m.int_parent_id is null or m.int_level = 1)");
		} else {
			$this->db->where("(m.int_parent_id = {$this->db->escape($parent_id)} and m.int_level > 1)");
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = $query->result();

			foreach ($data as $d) {
                if(empty($d->url)){
                    $tag .= '<tr><td><span class="tree-ml-'.$d->level.'">'.$d->nama.'</span></td>'.
                        '<td><div class="icheck-success d-inline"><input class="r_act" name="'.$d->menu_id.'[r]" value="1" type="checkbox" id="r_'.$d->menu_id.'"  '.(($d->r)? 'checked' : '').'><label for="r_'.$d->menu_id.'"></label></div></td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '<td>-</td>'.
                        '</tr>';
                }else{
                    $tag .= '<tr><td><span class="tree-ml-'.$d->level.'">'.$d->nama.'</span></td>'.
                        '<td><div class="icheck-success d-inline"><input class="r_act" name="'.$d->menu_id.'[r]" value="1" type="checkbox" id="r_'.$d->menu_id.'"  '.(($d->r)? 'checked' : '').'><label for="r_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="c_act" name="'.$d->menu_id.'[c]" value="1" type="checkbox" id="c_'.$d->menu_id.'"  '.(($d->c)? 'checked' : '').'><label for="c_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="u_act" name="'.$d->menu_id.'[u]" value="1" type="checkbox" id="u_'.$d->menu_id.'"  '.(($d->u)? 'checked' : '').'><label for="u_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-success d-inline"><input class="d_act" name="'.$d->menu_id.'[d]" value="1" type="checkbox" id="d_'.$d->menu_id.'"  '.(($d->d)? 'checked' : '').'><label for="d_'.$d->menu_id.'"></label></div></td>'.
                        '<td><div class="icheck-warning d-inline"><input class="all_line" value="'.$d->menu_id.'" type="checkbox" id="line_'.$d->menu_id.'"><label for="line_'.$d->menu_id.'"></label></div></td>'.
                        '</tr>';
                }

                $tag = $this->getMapMenu($group_id, $tag, $d->menu_id);
			}
		}
		return $tag;
	}

	public function setGroupMenu($group_id, $menu){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_group_menu} WHERE int_id_group = ?", [$group_id]);

		if(!empty($menu) && is_array($menu)){
			$ins = array();
			foreach($menu as $menu_id => $act){
				$ins[] = [	'int_id_group' => $group_id,
							'int_id_menu' => $menu_id,
							'c' => isset($act['c'])? 1 : 0,
							'r' => isset($act['r'])? 1 : 0,
							'u' => isset($act['u'])? 1 : 0,
							'd' => isset($act['d'])? 1 : 0,];
			}
			$this->db->insert_batch($this->s_group_menu, $ins);
		}
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}

	}
}
