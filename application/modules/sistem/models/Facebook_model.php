<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Facebook_model extends MY_Model {

	public function get_data(){
		return $this->db->query("SELECT * FROM {$this->m_facebook} WHERE int_id_facebook = 0")->row();
	}
	 
    public function update($upd){
        $this->db->where('int_id_facebook', 0)
                ->update($this->m_facebook, $upd);
    }

    public function check_fb($var_kode_pelaporan){
		$this->db->from("$this->t_pelaporan");
		$this->db->like('var_kode_pelaporan', $var_kode_pelaporan);
		$ret = $this->db->count_all_results();
		return $ret;
	}
}