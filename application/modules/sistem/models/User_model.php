<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class User_model extends MY_Model {

	public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("u.int_id_user as user_id, u.txt_username as username, CONCAT(u.txt_nama_depan, ' ', u.txt_nama_belakang) as nama, g.txt_nama as grup, u.int_status as is_aktif")
					->from($this->s_user. ' u')
					->join($this->s_group. ' g', 'g.int_id_group = u.int_id_group');

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('u.txt_username', $filter)
					->or_like('u.txt_nama_depan', $filter)
					->or_like('u.txt_nama_belakang', $filter)
					->group_end();
		}

		$order = 'u.txt_username ';
		switch($order_by){
			case 1 : $order = 'u.txt_username '; break;
			case 2 : $order = 'u.txt_nama_depan '; break;
			case 3 : $order = 'g.txt_nama '; break;
			case 4 : $order = 'u.int_status '; break;
			default : $order = 'u.int_id_user '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->s_user. ' u')
                 ->join($this->s_group. ' g', 'g.int_id_group = u.int_id_group');

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
                    ->like('u.txt_username', $filter)
                    ->or_like('u.txt_nama_depan', $filter)
                    ->or_like('u.txt_nama_belakang', $filter)
					->group_end();
		}
		return $this->db->count_all_results();
	}

	public function create($in){
		$lembaga_list = $in['int_id_lembaga'];
		//$sumber_list = $in['int_id_sumber'];

		$col['int_id_group']      = $in['group_id'];
        $col['txt_nama_depan']    = $in['first_name'];
        $col['txt_nama_belakang'] = $in['last_name'];
        $col['txt_username']     = $in['username'];
        $col['txt_password']     = password_hash($in['password'], PASSWORD_BCRYPT);
        $col['int_status']        = $in['is_aktif'];

        $this->db->trans_begin();
		$this->db->insert($this->s_user, $col);
		$user_id = $this->db->insert_id();

        if(!empty($lembaga_list)){
		    $inslembaga = [];
		    foreach($lembaga_list as $int_id_lembaga){
		        $inslembaga[] = ['int_id_user' => $user_id, 'int_id_lembaga' => $int_id_lembaga];
            }
		    $this->db->insert_batch($this->s_user_admin_lembaga, $inslembaga);
        }

        if(isset($in['int_id_sumber'])){
		    $inssumber = [];
		    foreach($sumber_list as $int_id_sumber){
		        $inssumber[] = ['int_id_user' => $user_id, 'int_id_sumber' => $int_id_sumber];
            }
		    $this->db->insert_batch($this->s_user_admin_sumber, $inssumber);
		}
		
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get($user_id){
		$data =  $this->db->query("	SELECT 	u.int_id_user as user_id, u.txt_username as username, u.txt_nama_depan as first_name, u.txt_nama_belakang as last_name, u.int_id_group as group_id, u.int_status as is_aktif 
									FROM	{$this->s_user} u  
									WHERE	u.int_id_user = ?", [$user_id])->row_array();

		if(!empty($data)){
			$arr_lembaga  = $this->db->query("SELECT ml.*
										FROM {$this->s_user_admin_lembaga} ual
										LEFT JOIN {$this->m_lembaga}  ml ON ual.`int_id_lembaga` = ml.`int_id_lembaga`
										WHERE ual.int_id_user = ?", [$user_id])->result();
		}

		if(!empty($data)){
			$arr_sumber  = $this->db->query("SELECT ms.*
										FROM {$this->s_user_admin_sumber} uas
										LEFT JOIN {$this->m_sumber}  ms ON uas.`int_id_sumber` = ms.`int_id_sumber`
										WHERE uas.int_id_user = ?", [$user_id])->result();
		}

		return (object) array_merge($data, ['arr_sumber' => $arr_sumber], ['arr_lembaga' => $arr_lembaga]);									
	}

	public function update($user_id, $in){
		$lembaga_list = $in['int_id_lembaga'];
		//$sumber_list = $in['int_id_sumber'];
		if(!empty($in['password'])){
			$col['txt_password'] = password_hash($in['password'],PASSWORD_BCRYPT);
		}
        $col['int_id_group']      = $in['group_id'];
        $col['txt_nama_depan']    = $in['first_name'];
        $col['txt_nama_belakang'] = $in['last_name'];
        $col['txt_username']     = $in['username'];
        $col['int_status']        = $in['is_aktif'];


		$this->db->trans_begin();

		$this->db->where('int_id_user', $user_id);
		$this->db->update($this->s_user, $col);

		$this->db->query("DELETE FROM {$this->s_user_admin_lembaga} WHERE int_id_user = ?", [$user_id]);
        if(!empty($lembaga_list)){
		    $inslembaga = [];
		    foreach($lembaga_list as $int_id_lembaga){
		        $inslembaga[] = ['int_id_user' => $user_id, 'int_id_lembaga' => $int_id_lembaga];
            }
		    $this->db->insert_batch($this->s_user_admin_lembaga, $inslembaga);
        }

		$this->db->query("DELETE FROM {$this->s_user_admin_sumber} WHERE int_id_user = ?", [$user_id]);
        if(isset($in['int_id_sumber'])){
		    $inssumber = [];
		    foreach($in['int_id_sumber'] as $int_id_sumber){
		        $inssumber[] = ['int_id_user' => $user_id, 'int_id_sumber' => $int_id_sumber];
            }
		    $this->db->insert_batch($this->s_user_admin_sumber, $inssumber);
        }

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($user_id){
	    if($user_id < 2) return false;

		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_user} WHERE int_id_user = ?", [$user_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	/*
	SELECT  TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME
FROM    KEY_COLUMN_USAGE
WHERE   REFERENCED_TABLE_NAME = 's_user' AND REFERENCED_COLUMN_NAME = 'user_id';
*/
}
