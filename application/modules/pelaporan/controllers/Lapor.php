<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Lapor extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAPOR'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lapor';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lapor_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
		$this->load->model('master/sumber_model', 'sumber');
		$this->load->model('master/lembaga_model', 'lembaga');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'pelaporan';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lapor';
		$this->breadcrumb->title = 'Pelaporan';
		$this->breadcrumb->card_title = 'Daftar Pelaporan';
		$this->breadcrumb->icon = 'fas fa-exclamation-triangle';
		$this->breadcrumb->list = ['Content', 'Data Pelaporan'];
		$this->css = true;
		$this->js = true;
		$lembaga_all = '';
		$get_lembaga_user = $this->lembaga->get_list_user();
			foreach($get_lembaga_user as $glu){
				$lembaga_all .=  "'".$glu->int_id_lembaga."',";
			}
		$data['lembaga_all'] = substr($lembaga_all,0,-1);
		if ($data['lembaga_all'] == ''){
			$data['lembaga_list'] = $this->lembaga->get_list();
		}else{
			$data['lembaga_list'] = $this->lembaga->get_list_user();
		}
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('lapor/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$sumber_all = '';
		$get_sumber_user = $this->sumber->get_list_user();
			foreach($get_sumber_user as $gsu){
				$sumber_all .=  "'".$gsu->int_id_sumber."',";
			}
		$sumber = substr($sumber_all,0,-1);

		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), $sumber, $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), $sumber, $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$tujuan_arr = $this->model->get_pelaporan_lembaga($d->int_id_pelaporan);
			$lembaga = '';
			if(!empty($tujuan_arr)){
				$lembaga .= '<span><b>Tujuan Pelaporan :</b></span><ul style="list-style: circle;padding-left: 15px;">';
				foreach($tujuan_arr as $tujuan){
					$lembaga .= '<li>'.$tujuan['txt_lembaga'].'</li>';
				}
				$lembaga .= '<ul>';
			}
			if($d->int_type == 1){
				$url = '<a href="'.$d->txt_url.'" class="btn btn-sm btn-primary" target="_blank"><i class="fas fa-external-link-alt"></i></a>';
			}else{ $url = ''; }
			$pelaporan = isset($d->txt_judul_pelaporan)? '<b>'.$d->txt_judul_pelaporan.'</b><br>'.$d->txt_detil_pelaporan : $d->txt_detil_pelaporan;
			$pelaporan = $pelaporan.'<br>'.$lembaga;
			$data[] = array($i.'. ', idn_date($d->dt_tanggal_pelaporan, 'j F Y H:i:s'), $pelaporan, $d->int_status, $url, $d->int_id_pelaporan);
		}
		$this->set_json(array( 'stat' => TRUE,
								//'q' => $this->model->getQueryLog(),
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Data Pelaporan';
		$data['kategori']	= $this->kategori->get_list();
		$data['sumber']		= $this->sumber->get_list_user();
		$data['lembaga']	= $this->lembaga->get_list();
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('lapor/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		
		$this->form_validation->set_rules('int_id_sumber', 'Sumber Pelaporan', 'required');
		$this->form_validation->set_rules('var_kode_pelaporan', 'Kode Pelaporan', "is_unique_update[{$this->model->t_pelaporan}.var_kode_pelaporan.int_id_pelaporan.{$this->input->post('var_kode_pelaporan')}]|min_length[2]");
		$this->form_validation->set_rules('int_id_kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('txt_nama_pelapor', 'Nama Pelapor', 'required');
		$this->form_validation->set_rules('txt_judul_pelaporan', 'Judul Pelaporan', 'required');
		$this->form_validation->set_rules('txt_detil_pelaporan', 'Detil Pelaporan', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$is_cover		= null;
			$countFiles		= 0;
			$files 			= [];
			$is_cover	= $this->input->post('is_cover');
			$txt_desc	= $this->input->post('txt_desc');
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['txt_desc' => $txt_desc[$i], 'is_cover' => ($is_cover == $i),
																'dir' => $img_dir, 'int_source' => 0]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
            $check = $this->model->create($this->input->post(), $lampiran_insert);
			$this->set_json(array_merge([  'stat' => $check, 
								'mc' => true,
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}
	
	public function get($int_id_pelaporan){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_id_pelaporan);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => 'Sistem tidak dapat menemukan data yang dimaksud']],true);
		}else{
			$data['data']		= $res;
			$data['routeURL']	= $this->routeURL;
			$data['url']		= site_url("{$this->routeURL}/$int_id_pelaporan");
			$data['title']		= 'Ubah Data Pelaporan';
			$data['kategori']	= $this->kategori->get_list();
			$data['sumber']		= $this->sumber->get_list_user();
			$data['lembaga']	= $this->lembaga->get_list();
			$data['input_file_name'] = $this->input_file_name;
			$this->load_view('lapor/index_action', $data);
		}
		
	}

	public function update($int_id_pelaporan){
		$this->authCheckDetailAccess('u');
		
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month."/";
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('int_id_sumber', 'Sumber Pelaporan', 'required');
		$this->form_validation->set_rules('int_id_kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('txt_nama_pelapor', 'Nama Pelapor', 'required');
		$this->form_validation->set_rules('txt_judul_pelaporan', 'Judul Pelaporan', 'required');
		$this->form_validation->set_rules('txt_detil_pelaporan', 'Detil Pelaporan', 'required');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$is_cover		= null;
			$countFiles		= 0;
			$files 			= [];
			$is_cover	= $this->input->post('is_cover');
			$txt_desc	= $this->input->post('txt_desc');
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['txt_desc' => $txt_desc[$i], 'is_cover' => (strcmp($is_cover, $i) == 0), 'dir' => $img_dir, 'int_source' => 0, 'cov' =>$is_cover, 'i' => $i]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				$check = $this->model->update($int_id_pelaporan, $this->input->post(), $lampiran_insert);
			}else{
				$check = $this->model->update($int_id_pelaporan, $this->input->post(), null, $txt_desc, $is_cover);

			}
			
			$this->set_json(array_merge([ 'stat' => $check, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}

	public function confirm($int_id_pelaporan){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_id_pelaporan);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_id_pelaporan/del");
			$data['title']	= 'Hapus Pelaporan';
			$data['info']   = ['Pelaporan' => $res->txt_judul_pelaporan];
			$this->load_view('lapor/index_delete', $data);
		}
	}

	public function delete($int_id_pelaporan){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_id_pelaporan);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function confirm_del_lampiran($int_id_pelaporan_img){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_id_pelaporan_img);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("lapor/$int_id_pelaporan_img/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('lapor/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_id_pelaporan_img){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_id_pelaporan_img);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
