<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Detil extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'DETIL'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'detil';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('detil_model', 'model');
		$this->load->model('lapor_model', 'lapor');
		$this->load->model('master/kategori_model', 'kategori');
		$this->load->model('master/sumber_model', 'sumber');
		$this->load->model('master/lembaga_model', 'lembaga');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'tanggapan';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'detil';
		$this->breadcrumb->title = 'Detil Pelaporan';
		$this->breadcrumb->card_title = 'Data Tanggapan';
		$this->breadcrumb->icon = 'fas fa-comment-dots';
		$this->breadcrumb->list = ['Pelaporan', 'Detil Tanggapan'];
		$this->css = true;
		$this->js = true;
		$data['kategori'] = $this->kategori->get_list();
		$lembaga_all = '';
		$get_lembaga_user = $this->lembaga->get_list_user();
			foreach($get_lembaga_user as $glu){
				$lembaga_all .=  "'".$glu->int_id_lembaga."',";
			}
		$data['lembaga_all'] = substr($lembaga_all,0,-1);
		if ($data['lembaga_all'] == ''){
			$data['lembaga_list'] = $this->lembaga->get_list();
		}else{
			$data['lembaga_list'] = $this->lembaga->get_list_user();
		}
		$this->render_view('detil/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$sumber_all = '';
		$get_sumber_user = $this->sumber->get_list_user();
			foreach($get_sumber_user as $gsu){
				$sumber_all .=  "'".$gsu->int_id_sumber."',";
			}
		$sumber = substr($sumber_all,0,-1);

		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->lapor->listCount($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), "", $filter_start, $filter_end, $this->input_post('search[value]', TRUE), TRUE);
		$ldata = $this->lapor->list($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), "", $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true), TRUE);

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$tujuan_arr = $this->lapor->get_pelaporan_lembaga($d->int_id_pelaporan);
			$lembaga = '';
			if(!empty($tujuan_arr)){
				$lembaga .= '<span><b>Tujuan Pelaporan :</b></span><ul style="list-style: circle;padding-left: 15px;">';
				foreach($tujuan_arr as $tujuan){
					$lembaga .= '<li>'.$tujuan['txt_lembaga'].'</li>';
				}
				$lembaga .= '</ul>';
			}

			if($d->int_type == 1){
				$url = '<a href="'.$d->txt_url.'" class="btn btn-sm btn-primary" target="_blank"><i class="fas fa-external-link-alt"></i></a>';
			}else{ $url = ''; }
			$pelaporan = isset($d->txt_judul_pelaporan)? '<b>'.$d->txt_judul_pelaporan.'</b><br>'.$d->txt_detil_pelaporan : $d->txt_detil_pelaporan;
			$pelaporan = $pelaporan.'<br>'.$lembaga;
			$data[] = array($i.'. ', idn_date($d->dt_tanggal_pelaporan, 'j F Y'), $pelaporan, $d->int_status, $url, $d->int_id_pelaporan);
		}
		$this->set_json(array( 'stat' => TRUE,
								'q' => $this->model->getQueryLog(),
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function get($int_id_pelaporan){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->lapor->get($int_id_pelaporan);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'pelaporan Not Found', 'message' => '']],true);
		}else{
			$this->page->subtitle = 'tanggapan';
			$this->page->menu 	  = 'pelaporan';
			$this->page->submenu1 = 'detil';
			$this->breadcrumb->title = 'Detil Pelaporan';
			$this->breadcrumb->card_title = 'Detil Pelaporan';
			$this->breadcrumb->icon = 'fas fa-comment-dots';
			$this->breadcrumb->list = ['Pelaporan', 'Data Pelaporan', 'Detil Pelaporan'];
			$this->css = true;
			$this->js = true;
			$data['data'] 	= $res;
			$data['url_dtable']	= site_url("{$this->routeURL}/tanggapan/$int_id_pelaporan");
			$data['url']	= site_url("{$this->routeURL}/tanggapan/$int_id_pelaporan/add");
			$this->render_view('detil/index_detail', $data, true);
		}
		
	}

	public function list_tanggapan($int_id_pelaporan){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$ldata = $this->model->list_tanggapan($int_id_pelaporan);

		foreach($ldata as $d){
			$get_lampiran = $this->model->get_lampiran_tanggapan($d->int_id_tanggapan);
			$lampiran = '';
			if(isset($get_lampiran)):
			$lampiran .= '<div class="card"><div class="card-body"><div class="row mb-1 img-popup-tanggapan">';
				foreach($get_lampiran as $lmp):
						if($lmp->int_source==1){
							$img_url = $lmp->txt_dir;
						}else{
							$img_url = cdn_url().$lmp->txt_dir;
						}
					$lampiran .= '<div class="col-md-2 text-center" data-responsive="'.$img_url.' 375, '.$img_url.' 480, '.$img_url.' 800" data-src="'.$img_url.'" data-sub-html="'.$lmp->txt_desc.'">
						<a href="'.$img_url.'">
							<img class="img-thumb-sm" src="'.$img_url.'">
						</a>
							<p>'.$lmp->txt_desc.'</p>
					</div>';
				endforeach;
				$lampiran .= '</div></div></div>';
			endif;
			//$pelaporan = $judul.$lembaga;*/
			$created = '';
			$updated = '';
			if(!empty($d->created)){
				$created = 'Dibuat oleh : '.$d->created;
			}
			if(!empty($d->updated)){
				$updated = 'Diubah oleh : '.$d->updated.' - '.idn_date($d->dt_updated);
			}
			$tanggapan = idn_date($d->dt_tanggal_tanggapan).'<br>'.$d->txt_detil_tanggapan.'<i style="border-top:solid 1px;">'.$created.$updated.'</i>';
			$data[] = array($tanggapan, $d->int_id_tanggapan);
		}
		$this->set_json(array( 'stat' => TRUE,
								//'q' => $this->model->getQueryLog(),
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add($int_id_pelaporan){ //tambah tanggapan
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['url'] = site_url("{$this->routeURL}/tanggapan/save");
		$data['int_id_pelaporan'] = $int_id_pelaporan;
		$data['title'] = 'Data Tanggapan';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('detil/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');
		
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month."/";
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('txt_detil_tanggapan', 'Detil Tanggapan', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$is_cover		= null;
			$countFiles		= 0;
			$files 			= [];
			$is_cover	= $this->input->post('is_cover');
			$txt_desc	= $this->input->post('txt_desc');
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['txt_desc' => $txt_desc[$i], 'is_cover' => ($is_cover == $i),
																'dir' => $img_dir, 'int_source' => 0]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
            $check = $this->model->create($this->input->post(), $lampiran_insert);
			$this->set_json(array_merge([  'stat' => $check, 
								'mc' => true,
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}
	
	public function get_img($int_id_tanggapan){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_tanggapan($int_id_tanggapan);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Mohon maaf !', 'title' => 'Lampiran atau gambar tidak tersedia', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['title']	= 'Lampiran Tanggapan';
			$this->load_view('detil/index_img', $data);
		}
		
	}

	public function get_tanggapan($int_id_tanggapan){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_tanggapan($int_id_tanggapan);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Tidak Ditemukan', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['routeURL']	= $this->routeURL.'/tanggapan/';
			$data['url']	= site_url("{$this->routeURL}/tanggapan/$int_id_tanggapan/upd");
			$data['title']	= 'Ubah Tanggapan';
			$data['input_file_name'] = $this->input_file_name;
			$this->load_view('detil/index_action', $data);
		}
		
	}
	
	public function update($int_id_tanggapan){
		$this->authCheckDetailAccess('u');
		
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month."/";
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('txt_detil_tanggapan', 'Detil Tanggapan', 'required');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$is_cover		= null;
			$countFiles		= 0;
			$files 			= [];
			$is_cover	= $this->input->post('is_cover');
			$txt_desc	= $this->input->post('txt_desc');
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['txt_desc' => $txt_desc[$i], 'is_cover' => (strcmp($is_cover, $i) == 0), 'dir' => $img_dir, 'int_source' => 0, 'cov' =>$is_cover, 'i' => $i]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				$check = $this->model->update($int_id_tanggapan, $this->input->post(), $lampiran_insert);
			}else{
				$check = $this->model->update($int_id_tanggapan, $this->input->post(), null, $txt_desc, $is_cover);

			}
			
			$this->set_json(array_merge([ 'stat' => $check, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}

	public function confirm($int_id_tanggapan){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_tanggapan($int_id_tanggapan, false);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/tanggapan/$int_id_tanggapan/del");
			$data['title']	= 'Hapus Tanggapan';
			$data['info']   = ['Detil Tanggapan' => $res->txt_detil_tanggapan];
			$this->load_view('detil/index_delete', $data);
		}
	}

	public function delete($int_id_tanggapan){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_id_tanggapan);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Berhasil Dihapus" : "Data Gagal Dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function confirm_del_lampiran($int_id_tanggapan_img){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_id_tanggapan_img);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("detil/tanggapan/$int_id_tanggapan_img/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('detil/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_id_tanggapan_img){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_id_tanggapan_img);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}	
}
