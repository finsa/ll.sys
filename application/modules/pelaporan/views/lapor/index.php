<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                    <div class="card-tools text-right">
                        <button type="button" data-block="body" class="btn btn-sm btn-primary ajax_modal" data-url="<?=$url?>" ><i class="fas fa-plus"></i> Tambah Pelaporan</button>
                    </div>
                </div><!-- /.card-header -->
                <div class="card-body p-0">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
                            <div class="col-md-3">
								<div class="form-group form-group-sm row text-sm mb-0">
									<label for="filter_date" class="col-md-3 col-form-label">Date</label>
									<div class="col-md-9">
										<input type="text" name="date_filter" class="form-control form-control-sm date_filter">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group row text-sm mb-0">
                                    <label for="lembaga_filter" class="col-sm-4 col-form-label">Lembaga/OPD</label>
                                    <div class="col-sm-8">
                                        <select id="lembaga_filter" name="lembaga_filter" class="form-control form-control-sm select2 lembaga_filter" style="width:100%">
                                            <option value="<?=$lembaga_all?>">- SEMUA OPD -</option>
                                            <?php 
                                                foreach($lembaga_list as $lem){
                                                    echo '<option value="'.$lem->int_id_lembaga.'">'.ucwords($lem->txt_lembaga).'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group row text-sm mb-0">
                                    <label for="status_filter" class="col-sm-4 col-form-label">Status</label>
                                    <div class="col-sm-8">
                                        <select id="status_filter" name="status_filter" class="form-control form-control-sm select2 status_filter" style="width:100%">
                                            <option value="">- Semua Status -</option>
                                            <option value="0">Belum Ditanggapi</option>
                                            <option value="1">Proses Penanganan</option>
                                            <option value="2">Dalam Perencanaan</option>
                                            <option value="3">Telah Diselesaikan</option>
                                            <option value="9">Belum Dimoderasi</option>
                                        </select>
                                    </div>
								</div>
							</div>
						</div>
                    </div>
                    <table class="table table-striped table-hover table-full-width" id="data_table">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tanggal</th>
                            <th>Pelaporan</th>
                            <th>Status</th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
