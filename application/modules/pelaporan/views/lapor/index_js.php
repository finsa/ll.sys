<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $('.lembaga_filter').select2();
    $('.status_filter').select2();


    var dataTable;
    $(document).ready(function() {
        dataTable = $('#data_table').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            //"order": [[ 1, "desc" ]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.lembaga_filter = $('.lembaga_filter').val();
					d.status_filter = $('.status_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "15",
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "100",
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSortable": false
                },
                {
                    "sWidth": "100",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "20",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "45",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [3],//post type
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span class="badge bg-danger">Belum Ditanggapi</span>';
                                break;
                            case 1:
                                return '<span class="badge bg-warning">Dalam Penanganan</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-primary">Perencanaan</span>';
                                break;
                            case 3:
                                return '<span class="badge bg-success">Selesai</span>';
                                break;
                            default:
                                return '<span class="badge bg-black">Belum Dimoderasi</span>';
                                break;
                        }
                    }
                },
                {
                    "aTargets": [5],
                    "mRender": function(data, type, row, meta) {
                        return  ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/get" class="ajax_modal btn btn-sm btn-warning tooltips" data-placement="top" data-original-title="Edit data" style="margin-left:-15px"><i class="fa fa-edit"></i></a> ') +
                                ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-sm btn-danger tooltips" data-placement="top" data-original-title="Hapus data" ><i class="fa fa-trash"></i></a> ');
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
		});

        $('.lembaga_filter').change(function(){
            dataTable.draw();
        });
		
        $('.status_filter').change(function(){
            dataTable.draw();
        });

        $.ajax({
            url: "<?=site_url("grab")?>",
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        });
    });
</script>