<?php
	if(isset($data->int_status)):
		switch ($data->int_status) {
			case 0:
				$status = '<span class="badge bg-danger">Belum Ditanggapi</span>';
				$color = 'danger';
				break;
			case 1:
				$status = '<span class="badge bg-warning">Dalam Penanganan</span>';
				$color = 'warning';
				break;
			case 2:
				$status = '<span class="badge bg-primary">Perencanaan</span>';
				$color = 'primary';
				break;
			case 3:
				$status = '<span class="badge bg-success">Selesai</span>';
				$color = 'success';
				break;
			default:
				$status = '<span class="badge bg-black">Belum Dimoderasi</span>';
				$color = 'secondary';
				break;
		}
	endif; ?>
<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-<?=$color?>">
				<div class="card-header" data-card-widget="collapse" style="cursor:pointer">
					<h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<dd><?=isset($data->txt_nama_pelapor)? $data->txt_nama_pelapor : $data->txt_sumber?>, <?=isset($data->dt_tanggal_pelaporan)? idn_date($data->dt_tanggal_pelaporan) : ''?></dd>
					<dt><?=isset($data->txt_judul_pelaporan)? $data->txt_judul_pelaporan : ''?></dt>
					<dd><?=isset($data->txt_detil_pelaporan)? $data->txt_detil_pelaporan : ''?></dd>
					<?php 
						if(!empty($data->lampiran)):?>

						<div class="card">
							<div class="card-body">
								<div class="row mb-1 img-popup">

								<?php foreach($data->lampiran as $lmp):
										if($lmp->int_source==1){
											$img_url = $lmp->txt_dir;
										}else{
											$img_url = cdn_url().$lmp->txt_dir;
										}?>
									<div class="col-md-2 text-center" data-responsive="<?=$img_url?> 375, <?=$img_url?> 480, <?=$img_url?> 800" data-src="<?=$img_url?>" data-sub-html="<?=$lmp->txt_desc?>">
										<a href="<?=$img_url?>">
											<img class="img-thumb-sm" src="<?=$img_url?>">
										</a>
											<p><?=$lmp->txt_desc?></p>
									</div>
								<?php endforeach; ?>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<?php $lembaga = '';
						if(!empty($data->arr_lembaga)):
							$lembaga .= '<span><b>Tujuan Pelaporan :</b></span><ul style="list-style: circle;padding-left: 15px;">';
							foreach($data->arr_lembaga as $al):
								$lembaga .= '<li>'.$al->txt_deskripsi.'</li>';
							endforeach;
							$lembaga .= '</ul>';
						endif; ?>
					<dd><?=$lembaga?></dd>
					<dd>
						<span><b>Status Pelaporan :</b></span>
						<ul style="list-style: none;padding-left: 0px;">
							<li><h6><?=$status?></h6></li>
						</ul>
					</dd>
				</div>
            </div>
        </section>
        <section class="col-lg-12">
            <div class="card">
				<div class="card-header" data-card-widget="collapse" style="cursor:pointer">
					<h3 class="card-title mt-1">
                        <i class="fas fa-comments"></i>
                        Data Tanggapan
                    </h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<table class="table table-striped table-hover table-full-width" id="data_table">

					</table>
					<button type="button" data-block="body" class="btn btn-sm btn-danger ajax_modal" data-url="<?=$url?>" style="float: right;"><i class="fas fa-plus"></i> Tambah Tanggapan</button>
				</div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
