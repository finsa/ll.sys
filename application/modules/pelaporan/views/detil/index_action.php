<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="posts-form" width="80%">
<div id="modal-posts" class="modal-dialog modal-92" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<ul class="nav nav-tabs no-border-b" id="custom-content-below-tab" role="tablist" style="width:100%">
				<li class="nav-item">
					<a class="nav-link active" id="posts-form-input-tab" data-toggle="pill" href="#posts-form-input" role="tab" aria-controls="posts-form-input" aria-selected="true"><?=$title?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="posts-form-lampiran-tab" data-toggle="pill" href="#posts-form-lampiran" role="tab" aria-controls="posts-form-lampiran" aria-selected="false">Lampiran Tanggapan</a>
				</li>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="tab-pane fade show active" id="posts-form-input" role="tabpanel" aria-labelledby="posts-form-input-tab">
					<div class="row">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">
									<div class="form-group row mb-1">
										<div class="col-sm-12">
											<label class="col-form-label">Detil Tanggapan</label>
											<textarea class="form-control form-control-sm textarea" name="txt_detil_tanggapan" id="txt_detil_tanggapan" placeholder="Enter text ..."><?=isset($data->txt_detil_tanggapan)? $data->txt_detil_tanggapan : ''?></textarea>
										</div>
									</div>
									<div class="form-group row mb-1">
										<div class="col-sm-7">
											<label class="col-form-label" id="label_sumber">URL/Link Tanggapan</label>
											<div class="input-group">
												<div class="input-group-prepend">
													<span class="input-group-text form-control-sm" id="icon_sumber"><i class="fas fa-link"></i></span>
												</div>
												<input type="text" id="txt_url" name="txt_url" class="form-control form-control-sm" value="<?=isset($data->txt_url)? $data->txt_url : ''?>">
											</div>
										</div>
										<div class="col-sm-3">
											<label class="col-form-label">Status Pelaporan</label>
											<select id="int_status" name="int_status" class="form-control form-control-sm select-2" style="width: 100%;">
												<option value="1">Proses Penanganan</option>
												<option value="2">Dalam Perencanaan</option>
												<option value="3">Telah Diselesaikan</option>
											</select>
										</div>
										<div class="col-sm-2">
											<label class="col-form-label">Waktu Tanggapan</label>
											<input type="text" class="form-control form-control-sm date_picker" id="dt_tanggal_tanggapan" name="dt_tanggal_tanggapan" value="<?=isset($data->dt_tanggal_tanggapan)? $data->dt_tanggal_tanggapan : date("Y-m-d H:i:s")?>"/>
										</div>
									</div>
									<input type="hidden" id="int_id_pelaporan" name="int_id_pelaporan" value="<?=isset($data->int_id_pelaporan)? $data->int_id_pelaporan : $int_id_pelaporan?>"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="posts-form-lampiran" role="tabpanel" aria-labelledby="posts-form-lampiran-tab">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row mb-1">
								<div class="col-md-12">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="<?=$input_file_name?>[]" id="customFile" accept="image/*" multiple>
										<label class="custom-file-label" for="customFile">Choose file</label>
									</div>
								</div>
							</div>
							<table class="table table-striped table-hover table-full-width">
							<thead>
							<tr>
								<th style="width:160px"></th>
								<th style="width:150px">Detil</th>
								<th>Deskripsi</th>
								<th class="text-center" style="width:100px">Cover</th>
								<th class="text-center" style="width:10px">#</th>
							</tr>
							</thead>
							<tbody class="preview-images-zone">
							<?php 
								if(isset($data->lampiran)):
									foreach($data->lampiran as $g):
							?>
								<tr class="img-server img-<?=$g->int_id_tanggapan_img?>">
									<td class="align-top">
										<div class="preview-image preview-show-<?=$g->int_id_tanggapan_img?> img-popup">
											<div class="image-zone" data-responsive="<?=cdn_url().$g->txt_dir ?> 375, <?=cdn_url().$g->txt_dir ?> 480, <?=cdn_url().$g->txt_dir ?> 800" data-src="<?=cdn_url().$g->txt_dir ?>" data-sub-html="<?=$g->txt_desc?>">
												<a href="<?=cdn_url().$g->txt_dir ?>">
													<img id="pro-img-<?=$g->int_id_tanggapan_img?>" class="img-thumb-sm" src="<?=cdn_url().$g->txt_dir ?>">
												</a>
											</div>
										</div>
									</td>
									<td class="align-top">
										<div><strong>Type : </strong><?=$g->txt_type?></div>
										<div><strong>Size : </strong><?=$g->txt_size?> Kb</div>
									</td>
									<td class="align-top">
										<input type="text" class="form-control form-control-sm title-image-<?=$g->int_id_tanggapan_img?>" placeholder="Description" name="txt_desc[<?=$g->int_id_tanggapan_img?>]" value="<?=$g->txt_desc?>"/>
									</td>
									<td class="text-center align-top">
										<div class="icheck-primary d-inline mr-2">
											<input type="radio" id="is_cover-<?=$g->int_id_tanggapan_img?>" name="is_cover" value="<?=$g->int_id_tanggapan_img?>" <?=($g->is_cover)? 'checked' : ''?> ><label for="is_cover-<?=$g->int_id_tanggapan_img?>"></label>
										</div>
									</td>
									<td class="text-center align-top">
										<a href="#" class="btn btn-sm btn-danger ajax_modal_confirm" data-url="<?=site_url($routeURL.$g->int_id_tanggapan_img.'/lampiran')?>" data-block="#modal-posts"><i class="fa fa-trash"></i> </a> 
									</td>
								</tr>
							
							<?php endforeach;
								endif;
							?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.textarea').summernote({
			dialogsInBody: true,
			height: 150,                 // set editor height
			minHeight: null,             // set minimum height of editor
			maxHeight: null,             // set maximum height of editor
			focus: true                  // set focus to editable area after initializing summernote
		});

		$(".img-popup").lightGallery();

		$('.select-2').select2({dropdownParent: $('#ajax-modal')});
		<?php if(isset($data->int_status)) echo '$("#int_status").val("'.$data->int_status.'").trigger("change");'?>

		$('.date_picker').daterangepicker(datepickModal);

		$("#posts-form").validate({
			rules: {
			    txt_detil_tanggapan:{
			        required: true,
					minlength: 20
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-posts';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#posts-form');
							dataTable.draw();
						}
						closeModal($modal, data);
						if(data.hasOwnProperty('files')){
							$.each(data.files, function(i, v){
								if(v.stat){
									if(!$('.'+v.cdImg).hasClass('is-valid')) $('.title-image-'+i).addClass('is-valid');
									if(!$('.'+v.cdImg).next('div').hasClass('valid-feedback'))$('.title-image-'+i).after('<div id="'+i+'-valid" class="valid-feedback">'+v.msg+'</div>');
								}else{
									if(!$('.'+v.cdImg).hasClass('is-invalid')) $('.'+v.cdImg).addClass('is-invalid');
									if(!$('.'+v.cdImg).next('div').hasClass('invalid-feedback'))$('.'+v.cdImg).after('<div id="'+i+'-error" class="invalid-feedback">'+v.msg+'</div>');
								}
								
								
							});
						}
						location.reload();
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
		
		bsCustomFileInput.init();
		$( ".preview-images-zone" ).sortable();
		$(document).on('click', '.image-cancel', function() {
			let no = $(this).data('no');
			//$(".preview-image.preview-show-"+no).parent().parent().remove();
			$(this).parent().parent().remove();
		});
		
		var num = 0;
		$('#customFile').change(function(){
			if (window.File && window.FileList && window.FileReader) {
			$('.preview-images-zone').find('tr.img-client').remove();
			var files = event.target.files; //FileList object
			for (let i = 0; i < files.length; i++) {
				var file = files[i];
				if (!file.type.match('image')) continue;
				
				var picReader = new FileReader();
				let cdImg 	= 'img'+getSandStr(3,'-',3);
				let imgSize = (file.size/1000).toFixed(2);
				let imgType = file.type;
				
				picReader.addEventListener('load', function (event) {
					var picFile = event.target;
					var html =  '<tr class="img-client"><td class="align-top">' +
								'<div class="preview-image preview-show-' + i + '">' +
								'<div class="image-zone"><img id="pro-img-' + i + '" src="' + picFile.result + '"></div>' +
								'<input type="hidden" name="cdImg['+i+']" value="'+cdImg+'"/></div></td>' +

								'<td class="align-top">' +
								'<div><strong>Tipe : </strong>'+imgType+'</div>'+
								'<div><strong>Ukuran : </strong>'+imgSize+' Kb</div>'+
								'</td>'+
								
								'<td class="align-top">' +
								'<input type="text" class="form-control form-control-sm title-image-'+i+' '+cdImg+'" placeholder="Keterangan" name="txt_desc['+i+']" value=""/></td>'+

								'<td class="text-center align-top">'+
									'<div class="icheck-primary d-inline mr-2">'+
										'<input type="radio" id="cover-'+i+'" name="is_cover" value="'+i+'" checked><label for="cover-'+i+'"></label>'+
									'</div>'+
								'</td>'+

								'<td class="text-center align-top"><div class="icheck-success d-inline"><input type="checkbox" name="img_select[]" value="'+i+'" id="gambar'+i+'" checked><label for="gambar'+i+'"></label></td></tr>';
					$(html).appendTo(".preview-images-zone");
				});

				//$( ".preview-images-zone" ).sortable();
				picReader.readAsDataURL(file);
			}
				$("#pro-image").val('');
			}else {
				alert('Browser not support');
			}
		});
	});
</script>