<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="lampiran-confirm" width="80%">
<div id="modal-lampiran" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
            <div class="form-delete-message text-center"></div>
            <div class="alert alert-default">
				<h5 class="text-warning"><i class="icon fas fa-ban"></i> Konfirmasi Hapus lampiran</h5>
				Apakah anda yakin menghapus data berikut:

				<div class="row">
					<div class="col-md-6">
						<div class="preview-images-zone img-<?=$data->int_id_tanggapan_img?>">
							<div class="preview-image preview-show-0">
								<div class="image-zone"><img id="pro-img-<?=$data->int_id_tanggapan_img?>" src="<?=cdn_url().$data->txt_dir ?>"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<dl class="row mb-0">
							<dt class="col-sm-2 text-right"><strong>Title:</strong></dt><dd class="col-sm-9 mb-0"><?=$data->txt_desc ?></dd>
							<dt class="col-sm-2 text-right"><strong>Type:</strong></dt><dd class="col-sm-9 mb-0"><?=$data->txt_type ?></dd>
							<dt class="col-sm-2 text-right"><strong>Size:</strong></dt><dd class="col-sm-9 mb-0"><?=$data->txt_size ?></dd>
						</dl>
					</div>
				</div>
            </div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-success">Keluar</button>
			<button type="submit" class="btn btn-danger">Hapus</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$("#lampiran-confirm").submit(function(){
            $('.form-delete-message').html('');
            blockUI(this);
            $(this).ajaxSubmit({
                dataType:  'json',
                data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content"), method: "DELETE"},
                success: function(data){
                    refreshToken(data);
                    unblockUI(this);
					setFormMessage('.form-delete-message', data);
                    if(data.stat){
						$('.img-<?=$data->int_id_tanggapan_img?>').remove();
					}
					setTimeout(function(){
						$('#ajax-modal-confirm').modal("hide");
					}, 1000);
					
                //closeModal($modalConfirm, data);
                }
            });
            return false;
		});
	});
</script>