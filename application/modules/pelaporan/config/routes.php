<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['lapor']['get']                          = 'pelaporan/lapor';
$route['lapor']['post']         			    = 'pelaporan/lapor/list';
$route['lapor/add']['get']      			    = 'pelaporan/lapor/add';
$route['lapor/save']['post']    			    = 'pelaporan/lapor/save';
$route['lapor/([0-9]+)/get']['get']             = 'pelaporan/lapor/get/$1';
$route['lapor/([0-9]+)']['get']                 = 'pelaporan/detil/get/$1';
$route['lapor/([0-9]+)']['post']                = 'pelaporan/lapor/update/$1';
$route['lapor/([0-9]+)/del']['get']             = 'pelaporan/lapor/confirm/$1';
$route['lapor/([0-9]+)/del']['post']            = 'pelaporan/lapor/delete/$1';
$route['lapor/([0-9]+)/lampiran']['get']        = 'pelaporan/lapor/confirm_del_lampiran/$1';
$route['lapor/([0-9]+)/lampiran_del']['post']   = 'pelaporan/lapor/delete_lampiran/$1';

$route['detil']['get']                          = 'pelaporan/detil';                    //index
$route['detil']['post']         			    = 'pelaporan/detil/list';               //list pelaporan
$route['detil/tanggapan/([0-9]+)']['post']      = 'pelaporan/detil/list_tanggapan/$1';  //list tanggapan
$route['detil/([0-9]+)']['get']     	        = 'pelaporan/detil/get/$1';             //detil pelaporan dan tanggapan
$route['detil/tanggapan/([0-9]+)/add']['get']   = 'pelaporan/detil/add/$1';             //tambah tanggapan - index_action
$route['detil/tanggapan/([0-9]+)/img']['get']   = 'pelaporan/detil/get_img/$1';             //tambah tanggapan - index_action
//$route['detil/([0-9]+)/det']['get']           = 'pelaporan/detil/detail/$1';          //detil tanggapan - index_action
$route['detil/tanggapan/save']['post']          = 'pelaporan/detil/save';            //simpan tanggapan baru

$route['detil/tanggapan/([0-9]+)']['get']       = 'pelaporan/detil/get_tanggapan/$1';    //detil tanggapan
$route['detil/tanggapan/([0-9]+)/upd']['post']  = 'pelaporan/detil/update/$1';
$route['detil/tanggapan/([0-9]+)/del']['get']   = 'pelaporan/detil/confirm/$1';
$route['detil/tanggapan/([0-9]+)/del']['post']  = 'pelaporan/detil/delete/$1';
$route['detil/tanggapan/([0-9]+)/lampiran']['get']      = 'pelaporan/detil/confirm_del_lampiran/$1';
$route['detil/tanggapan/([0-9]+)/lampiran_del']['post'] = 'pelaporan/detil/delete_lampiran/$1';

