<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lapor_model extends MY_Model {

    public function list($lembaga_filter = "",  $status_filter = "", $sumber_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0, $tanggapan = false){
		$this->db->select("pel.`int_id_pelaporan`, pel.`txt_judul_pelaporan`, pel.`txt_detil_pelaporan`, pel.`dt_tanggal_pelaporan`, pel.`txt_url`, pel.`int_status`, pel.`int_id_sumber`, pel.`txt_url`, lem.`int_id_lembaga`, lem.`txt_nama_lembaga`, sum.`int_type`, sum.`txt_icon`")
					->from($this->t_pelaporan." pel")
					->join("t_pelaporan_lembaga pl", "pel.`int_id_pelaporan` =  pl.`int_id_pelaporan`", "left")
					->join("m_lembaga lem", "pl.`int_id_lembaga` = lem.`int_id_lembaga`", "left")
					->join("m_sumber sum", "pel.`int_id_sumber` = sum.`int_id_sumber`", "left")
					->group_by("pel.`int_id_pelaporan`");

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_tanggal_pelaporan', $filter_start, $filter_end);
		}
		
		if($tanggapan == true){ // filter
            $this->db->where('pel.`int_status` != 9 ');
		}

		if(($lembaga_filter!="")){ // filter
            $this->db->where('lem.`int_id_lembaga` IN ('.$lembaga_filter.')');
            $this->db->where('pel.`int_status` != 9 ');
		}

		if($sumber_filter!=""){ // filter
            $this->db->where('sum.`int_id_sumber` IN ('.$sumber_filter.')');
		}

		if($status_filter!=""){ // filter
            $this->db->where('pel.`int_status`', ($status_filter));
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_judul_pelaporan', $filter)
					->or_like('txt_detil_pelaporan', $filter)
					->group_end();
		}

		$order = 'dt_tanggal_pelaporan';
		switch($order_by){
			case 1 : $order = 'dt_tanggal_pelaporan '; break;
			case 2 : $order = 'txt_judul_pelaporan '; break;
			default: {$order = 'dt_tanggal_pelaporan '; $sort = 'DESC';} break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}


		return $this->db->order_by($order, $sort)->get()->result();

	}
	
	public function listCount($lembaga_filter = "",  $status_filter = "", $sumber_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $tanggapan = false){
		$this->db->select("pel.`int_id_pelaporan`, pel.`txt_judul_pelaporan`, pel.`txt_detil_pelaporan`, pel.`dt_tanggal_pelaporan`, pel.`int_status`, pel.`int_id_sumber`, pel.`txt_url`, lem.`int_id_lembaga`, lem.`txt_nama_lembaga`")
					->from($this->t_pelaporan." pel")
					->join("t_pelaporan_lembaga pl", "pel.`int_id_pelaporan` =  pl.`int_id_pelaporan`", "left")
					->join("m_lembaga lem", "pl.`int_id_lembaga` = lem.`int_id_lembaga`", "left")
					->join("m_sumber sum", "pel.`int_id_sumber` = sum.`int_id_sumber`", "left")
					->group_by("pel.`int_id_pelaporan`");

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_tanggal_pelaporan', $filter_start, $filter_end);
		}
		
		if($tanggapan == true){ // filter
            $this->db->where('pel.`int_status` != 9 ');
		}

		if(($lembaga_filter!="")){ // filter
			$this->db->where('lem.`int_id_lembaga` IN ('.$lembaga_filter.')');
			$this->db->where('pel.`int_status` != 9 ');
		}

		if($sumber_filter!=""){ // filter
			$this->db->where('sum.`int_id_sumber` IN ('.$sumber_filter.')');
		}

		if($status_filter!=""){ // filter
			$this->db->where('pel.`int_status`', ($status_filter));
		}

		if(!empty($filter)){ // filters
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_judul_pelaporan', $filter)
					->or_like('txt_detil_pelaporan', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function get_pelaporan_lembaga($int_id_pelaporan){
		return $this->db->query("SELECT lem.`txt_deskripsi` AS `txt_lembaga`
								FROM  {$this->t_pelaporan_lembaga} pl
								LEFT JOIN {$this->m_lembaga} lem ON pl.`int_id_lembaga` = lem.`int_id_lembaga`
								WHERE pl.`int_id_pelaporan` = {$int_id_pelaporan}")->result_array();
	}

	public function create($ins, $lampiran = []){
		$lembaga_list = $ins['int_id_lembaga'];
		$ins['var_kode_pelaporan'] = $ins['var_kode_pelaporan'].date("ymdHis");
		if(isset($ins['created_by'])){
			$ins['created_by'] = $this->session->userdata['user_id'];
		}
		if($ins['int_id_kategori'] == 6){
			$ins['int_status'] = 3;
		}
		$this->db->trans_begin();
		$ins = $this->clearFormInsert($ins, ['int_id_lembaga', 'lampiran', 'is_cover', 'txt_desc', 'img_select', 'cms_token', 'cdImg', 'int_source', 'txt_prefix']);

		$this->db->insert($this->t_pelaporan, $ins);
		$int_id_pelaporan = $this->db->insert_id();
		$this->setQueryLog('ins_pelaporan');

		if(!empty($lembaga_list)){
		    $inslembaga = [];
		    foreach($lembaga_list as $int_id_lembaga){
		        $inslembaga[] = ['int_id_pelaporan' => $int_id_pelaporan, 'int_id_lembaga' => $int_id_lembaga];
            }
		    $this->db->insert_batch($this->t_pelaporan_lembaga, $inslembaga);
        }

		if(!empty($lampiran)){
			print_r($lampiran);
			$lampiran_insert = [];
			foreach($lampiran as $gal){
				$lampiran_insert[] = ['int_id_pelaporan' => $int_id_pelaporan,
									'txt_dir' => $this->db->escape($gal['dir'].$gal['file_name']),
									'txt_desc' => $this->db->escape($gal['txt_desc']),
									'is_cover' => $this->db->escape($gal['is_cover']),
									'txt_size' => $this->db->escape(round($gal['file_size'],2)),
									'txt_type' => $this->db->escape($gal['file_type']),
									'int_sources' => $this->db->escape($gal['int_source'])];
			}
			$this->db->insert_batch($this->t_pelaporan_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

	public function get($int_id_pelaporan, $with_lampiran = true){
		$data = $this->db->query("SELECT mk.`txt_kategori`, ms.`txt_sumber`, tp.*
								FROM {$this->t_pelaporan}  tp
								LEFT JOIN {$this->m_kategori}  mk ON tp.`int_id_kategori` = mk.`int_id_kategori`
								LEFT JOIN {$this->m_sumber}  ms ON tp.`int_id_sumber` = ms.`int_id_sumber`
								WHERE tp.`int_id_pelaporan` = {$int_id_pelaporan}")->row_array();
		if(!empty($data)){
			$arr_lembaga  = $this->db->query("SELECT ml.`txt_deskripsi`,  ml.`int_id_lembaga`
												FROM {$this->t_pelaporan_lembaga} tpl
												LEFT JOIN {$this->m_lembaga}  ml ON tpl.`int_id_lembaga` = ml.`int_id_lembaga`
												WHERE int_id_pelaporan = ?", [$int_id_pelaporan])->result();
		}

		$lampiran = ($with_lampiran)? $this->get_lampiran($int_id_pelaporan) : null;
		return (object) array_merge($data, ['lampiran' => $lampiran], ['arr_lembaga' => $arr_lembaga]);
	}

	public function get_lampiran($int_id_pelaporan){
		return $this->db->query("SELECT *
								FROM  {$this->t_pelaporan_img} 
								WHERE int_id_pelaporan = {$int_id_pelaporan}")->result();
			
	}

	public function update($int_id_pelaporan, $upd, $lampiran = [], $txt_desc = [], $is_cover = null){
		$lembaga_list = $upd['int_id_lembaga'];
		$upd['dt_updated'] = date('Y-m-d H:i:s');
		$upd['updated_by'] = $this->session->userdata['user_id'];

		
		$this->db->trans_begin();

		$upd = $this->clearFormInsert($upd, ['int_id_lembaga', 'lampiran', 'is_cover', 'txt_desc', 'img_select', 'cms_token', 'cdImg', 'int_source', 'txt_prefix']);
		//print_r($lampiran);

		$this->db->where('int_id_pelaporan', $int_id_pelaporan);
		$this->db->update($this->t_pelaporan, $upd);
		$this->setQueryLog('upd_pelaporan');
		
		$this->db->query("DELETE FROM {$this->t_pelaporan_lembaga} WHERE int_id_pelaporan = ?", [$int_id_pelaporan]);
        if(!empty($lembaga_list)){
		    $inslembaga = [];
		    foreach($lembaga_list as $int_id_lembaga){
		        $inslembaga[] = ['int_id_pelaporan' => $int_id_pelaporan, 'int_id_lembaga' => $int_id_lembaga];
            }
		    $this->db->insert_batch($this->t_pelaporan_lembaga, $inslembaga);
        }

		if(!empty($lampiran)){
			$ord = 0;
			$lampiran_insert = [];
			foreach($lampiran as $gal){
				$lampiran_insert[] = ['int_id_pelaporan' => $int_id_pelaporan,
									'txt_dir' => $this->db->escape($gal['dir'].$gal['file_name']),
									'txt_desc' => $this->db->escape($gal['txt_desc']),
									'is_cover' => $this->db->escape($gal['is_cover']),
									'int_order' => $this->db->escape($ord),
									'txt_size' => $this->db->escape(round($gal['file_size'],2)),
									'txt_type' => $this->db->escape($gal['file_type']),
									'int_source' => $this->db->escape($gal['int_source'])];
				$ord++;
			}
			$this->db->insert_batch($this->t_pelaporan_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
		
		if(!empty($txt_desc)){
			$o = 0;
			foreach($txt_desc as $id=>$val){
				$this->db->query("UPDATE {$this->t_pelaporan_img} SET txt_desc = ?, is_cover = ?, int_order = ? WHERE int_id_pelaporan_img = {$id} ", [$val, ($id == $is_cover)? 1 : 0, $o]);
				$o++;
			}
			$this->setQueryLog('upd_lampiran');
        }

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_id_pelaporan){
		$this->db->trans_begin();
		$this->db->delete($this->t_pelaporan,  ['int_id_pelaporan' => $int_id_pelaporan]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	public function get_lampiran_img($int_id_pelaporan_img){
		return $this->db->query("SELECT *
								FROM 	{$this->t_pelaporan_img} 
								WHERE 	int_id_pelaporan_img = {$int_id_pelaporan_img}")->row();
	}
	
	public function delete_lampiran_img($int_id_pelaporan_img){
		$data = $this->get_lampiran_img($int_id_pelaporan_img);
		if(!empty($data)){
			$cdn_dir = $this->config->item('upload_path');
			$img_dir = $this->config->item('img_dir');
			$unlink_dir = str_replace($img_dir,"",$cdn_dir);
			if($data->int_source == 0){
				unlink(FCPATH.'/'.$unlink_dir.$data->txt_dir);
			}
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->t_pelaporan_img} WHERE int_id_pelaporan_img = {$int_id_pelaporan_img}");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}
}
