<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Detil_model extends MY_Model {

    public function list_tanggapan($int_id_pelaporan){
		$this->db->select("tt.*, CONCAT(cr.txt_nama_depan, ' ', cr.txt_nama_belakang) as created, CONCAT(up.txt_nama_depan, ' ', up.txt_nama_belakang) as updated")
					->from($this->t_tanggapan ." tt")
					->join($this->s_user." cr", "tt.created_by = cr.int_id_user", "left")
					->join($this->s_user." up", "tt.updated_by = up.int_id_user", "left")
					->where("int_id_pelaporan", $int_id_pelaporan);

		return $this->db->order_by('tt.dt_tanggal_tanggapan', 'DESC')->get()->result();

	}

	public function get_tanggapan($int_id_tanggapan, $with_lampiran = true){
		$data = $this->db->query("SELECT *
								FROM {$this->t_tanggapan}
								WHERE `int_id_tanggapan` = {$int_id_tanggapan}")->row_array();

		$lampiran = ($with_lampiran)? $this->get_lampiran_tanggapan($int_id_tanggapan) : null;
		return (object) array_merge($data, ['lampiran' => $lampiran]);
	}

	public function get_lampiran_tanggapan($int_id_tanggapan){
		return $this->db->query("SELECT *
								FROM  {$this->t_tanggapan_img} 
								WHERE int_id_tanggapan = {$int_id_tanggapan}")->result();
			
	}

	public function create($ins, $lampiran = []){
		$this->db->trans_begin();
		$ins['created_by'] = $this->session->userdata['user_id'];
		$ins['var_kode_tanggapan'] = "FC".date("ymdHis");
		$ins = $this->clearFormInsert($ins, ['lampiran', 'is_cover', 'txt_desc', 'img_select', 'cms_token', 'cdImg', 'int_source']);

		$this->db->insert($this->t_tanggapan, $ins);
		$int_id_tanggapan = $this->db->insert_id();
		$this->setQueryLog('ins_tanggapan');

		if(!empty($lampiran)){
			//print_r($lampiran);
			$lampiran_insert = [];
			foreach($lampiran as $gal){
				$lampiran_insert[] = ['int_id_tanggapan' => $int_id_tanggapan,
									'txt_dir' => $this->db->escape($gal['dir'].$gal['file_name']),
									'txt_desc' => $this->db->escape($gal['txt_desc']),
									'is_cover' => $this->db->escape($gal['is_cover']),
									'txt_size' => $this->db->escape(round($gal['file_size'],2)),
									'txt_type' => $this->db->escape($gal['file_type']),
									'int_source' => $this->db->escape($gal['int_source'])];
			}
			$this->db->insert_batch($this->t_tanggapan_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
		$update_pelaporan_status = $this->update_pelaporan_status($ins['int_id_pelaporan'], $ins['int_status']);
		if ($this->db->trans_status() === FALSE && $update_pelaporan_status === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

	public function update($int_id_tanggapan, $upd, $lampiran = [], $txt_desc = [], $is_cover = null){
		$int_id_pelaporan = $upd['int_id_pelaporan'];
		$upd['dt_updated'] = date('Y-m-d H:i:s');
		$upd['updated_by'] = $this->session->userdata['user_id'];
		
		$this->db->trans_begin();

		$upd = $this->clearFormInsert($upd, ['lampiran', 'is_cover', 'txt_desc', 'img_select', 'cms_token', 'cdImg', 'int_source']);
		//print_r($lampiran);

		$this->db->where('int_id_tanggapan', $int_id_tanggapan);
		$this->db->update($this->t_tanggapan, $upd);
		$this->setQueryLog('upd_tanggapan');
		
		if(!empty($lampiran)){
			$ord = 0;
			$lampiran_insert = [];
			foreach($lampiran as $gal){
				$lampiran_insert[] = ['int_id_tanggapan' => $int_id_tanggapan,
									'txt_dir' => $this->db->escape($gal['dir'].$gal['file_name']),
									'txt_desc' => $this->db->escape($gal['txt_desc']),
									'is_cover' => $this->db->escape($gal['is_cover']),
									'int_order' => $this->db->escape($ord),
									'txt_size' => $this->db->escape(round($gal['file_size'],2)),
									'txt_type' => $this->db->escape($gal['file_type']),
									'int_source' => $this->db->escape($gal['int_source'])];
				$ord++;
			}
			$this->db->insert_batch($this->t_tanggapan_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
		
		if(!empty($txt_desc)){
			$o = 0;
			foreach($txt_desc as $id=>$val){
				$this->db->query("UPDATE {$this->t_tanggapan_img} SET txt_desc = ?, is_cover = ?, int_order = ? WHERE int_id_tanggapan_img = {$id} ", [$val, ($id == $is_cover)? 1 : 0, $o]);
				$o++;
			}
			$this->setQueryLog('upd_lampiran');
        }

		$update_pelaporan_status = $this->update_pelaporan_status($upd['int_id_pelaporan'], $upd['int_status']);
		if ($this->db->trans_status() === FALSE && $update_pelaporan_status === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function update_pelaporan_status($int_id_pelaporan, $int_status){
		$this->db->trans_begin();
		$upd['int_status'] = $int_status;
		$this->db->where('int_status < '.$int_status);
		$this->db->where('int_id_pelaporan', $int_id_pelaporan);
		$this->db->update($this->t_pelaporan, $upd);
		$this->setQueryLog('upd_status_pelaporan');

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	
	public function delete($int_id_tanggapan){
		$this->db->trans_begin();
		$this->db->delete($this->t_tanggapan,  ['int_id_tanggapan' => $int_id_tanggapan]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get_lampiran_img($int_id_tanggapan_img){
		return $this->db->query("SELECT *
								FROM 	{$this->t_tanggapan_img} 
								WHERE 	int_id_tanggapan_img = {$int_id_tanggapan_img}")->row();
	}
	
	public function delete_lampiran_img($int_id_tanggapan_img){
		$data = $this->get_lampiran_img($int_id_tanggapan_img);
		if(!empty($data)){
			$cdn_dir = $this->config->item('upload_path');
			$img_dir = $this->config->item('img_dir');
			$unlink_dir = str_replace($img_dir,"",$cdn_dir);
			unlink(FCPATH.'/'.$unlink_dir.$data->txt_dir);
			
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->t_tanggapan_img} WHERE int_id_tanggapan_img = {$int_id_tanggapan_img}");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}
}
