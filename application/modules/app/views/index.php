<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 col-12">
            <div class="small-box bg-green">
                <div class="inner">
                    <h1><?=$lap_hariini?></h1>
                    <h4>Pelaporan Hari Ini</h4>
                </div>
                <div class="icon">
                    <i class="fas fa-clock"></i>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="small-box bg-teal">
                <div class="inner">
                    <h1><?=$lap_mingguini?></h1>
                    <h4>Pelaporan Minggu Ini</h4>
                </div>
                <div class="icon">
                    <i class="fas fa-calendar-week"></i>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="small-box bg-cyan">
                <div class="inner">
                    <h1><?=$lap_bulanini?></h1>
                    <h4>Pelaporan Bulan Ini</h4>
                </div>
                <div class="icon">
                    <i class="fas fa-calendar-alt"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-outline card-success">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-calendar-week"></i>
                Rekapitulasi Pelaporan Mingguan
            </h5>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-exclamation-triangle"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Belum Ditanggapi</span>
                            <span class="info-box-number"><?=$lap_mingguini0?></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-hourglass-half"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Proses Penanganan</span>
                            <span class="info-box-number"><?=$lap_mingguini1?></span>
                        </div>
                    </div>
                </div>
                <div class="clearfix hidden-md-up"></div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-primary elevation-1"><i class="fas fa-calendar-alt"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Dalam Perencanaan</span>
                            <span class="info-box-number"><?=$lap_mingguini2?></span>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-3">
                    <div class="info-box mb-3">
                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-check-double"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Telah Diselesaikan</span>
                            <span class="info-box-number"><?=$lap_mingguini3?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-outline card-danger">
        <div class="card-header">
            <h5 class="card-title">
                <i class="fas fa-calendar-alt"></i>
                Rekapitulasi Pelaporan Bulanan
            </h5>
        </div>
        <div class="card-body">
        <div class="row">
            <div class="col-lg-3 col-6">
                <div class="small-box bg-danger">
                    <div class="inner">
                        <h3><?=$lap_bulanini0?></h3>
                        <p>Belum Ditanggapi</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-exclamation-triangle"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-warning">
                    <div class="inner">
                        <h3><?=$lap_bulanini1?></h3>
                        <p>Proses Penanganan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-hourglass-half"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-primary">
                    <div class="inner">
                        <h3><?=$lap_bulanini2?></h3>
                        <p>Dalam Perencanaan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-calendar-alt"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-6">
                <div class="small-box bg-success">
                    <div class="inner">
                        <h3><?=$lap_bulanini3?></h3>
                        <p>Telah Diselesaikan</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-check-double"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>