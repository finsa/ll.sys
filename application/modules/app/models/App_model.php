<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class App_model extends MY_Model {

	public function get_lap_hariini(){
		//$add_date = date('Y-m-d', strtotime('1900-01-01'. ' + '.$int_date.' days'));

        $this->db->from($this->t_pelaporan)
                ->where("dt_tanggal_pelaporan BETWEEN '".date("Y-m-d 00:00:00")."' AND '".date("Y-m-d 23:59:59")."'");
        return $this->db->count_all_results();
    }

	public function get_lap_mingguini($status = ""){
		$start_date = date('Y-m-d 00:00:00', strtotime(date("Y-m-d 00:00:00"). ' - 6 days'));

        $this->db->from($this->t_pelaporan)
                ->where("dt_tanggal_pelaporan BETWEEN '".$start_date."' AND '".date("Y-m-d 23:59:59")."'");

        if($status != ""){ // filter
            $this->db->where('int_status', $status);
        }
        
        return $this->db->count_all_results();
    }

	public function get_lap_bulanini($status = ""){
		$start_date = date('Y-m-d 00:00:00', strtotime(date("Y-m-d 00:00:00"). ' - 29 days'));

        $this->db->from($this->t_pelaporan)
                ->where("dt_tanggal_pelaporan BETWEEN '".$start_date."' AND '".date("Y-m-d 23:59:59")."'");

        if($status != ""){ // filter
            $this->db->where('int_status', $status);
        }

        return $this->db->count_all_results();
    }
}