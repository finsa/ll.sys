<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {
    private $mode = ['Hari Ini', 'Minggu Ini', 'Bulan Ini'];
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'APP-HOME';
		$this->authCheck();
		
		$this->load->model('app_model', 'model');
    }
	
	public function index(){
		$this->page->subtitle = 'Dashboard';
		$this->page->menu = 'dashboard';
        $this->breadcrumb->title = 'Dashboard';;
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        //$this->js_data = ['stasiun' => $this->model->get_stasiun()];
        $data['lap_hariini']   = $this->model->get_lap_hariini();
        $data['lap_mingguini']   = $this->model->get_lap_mingguini();
        $data['lap_mingguini0']   = $this->model->get_lap_mingguini('0');
        $data['lap_mingguini1']   = $this->model->get_lap_mingguini('1');
        $data['lap_mingguini2']   = $this->model->get_lap_mingguini('2');
        $data['lap_mingguini3']   = $this->model->get_lap_mingguini('3');
        $data['lap_bulanini']   = $this->model->get_lap_bulanini();
        $data['lap_bulanini0']   = $this->model->get_lap_bulanini('0');
        $data['lap_bulanini1']   = $this->model->get_lap_bulanini('1');
        $data['lap_bulanini2']   = $this->model->get_lap_bulanini('2');
        $data['lap_bulanini3']   = $this->model->get_lap_bulanini('3');
		$this->render_view('index', $data, false);
	}

}
