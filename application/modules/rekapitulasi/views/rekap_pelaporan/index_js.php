<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
<script>
    $('.lembaga_filter').select2();
    $('.status_filter').select2();

    function exportData(th){
        $('.form-message').html('');
        let blc = $(th).data('block');
        blockUI(blc);
        $.AjaxDownloader({
            url  : $(th).data('url'),
            data : {
                <?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content"),
                date_filter : $('.date_filter').val(),
                lembaga_filter : $('.lembaga_filter').val()
            }
        });
            setTimeout(function(){unblockUI(blc)}, 3000);
    }

    var dataTable;
    $(document).ready(function() {
        dataTable = $('#data_table').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "paging":   false,
            "searching": false,
            "ordering": false,
            "info": false,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            //"order": [[ 1, "desc" ]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.date_filter = $('.date_filter').val();
					d.lembaga_filter = $('.lembaga_filter').val();
					d.status_filter = $('.status_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "2",
                    "sClass": "text-right vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "60",
                    "sClass": "text-right vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "25%",
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "25%",
                    "sClass": "vertical-top",
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                },
				{
                    "sWidth": "auto",
                    "sClass": "vertical-top",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [7],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span class="badge bg-danger">Belum Ditanggapi</span>';
                                break;
                            case 1:
                                return '<span class="badge bg-warning">Dalam Penanganan</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-primary">Perencanaan</span>';
                                break;
                            case 3:
                                return '<span class="badge bg-success">Selesai</span>';
                                break;
                            default:
                                return '<span class="badge bg-black">Belum Dimoderasi</span>';
                                break;
                        }
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
		});

        $('.lembaga_filter').change(function(){
            dataTable.draw();
        });
		
        $('.status_filter').change(function(){
            dataTable.draw();
        });

    });
</script>