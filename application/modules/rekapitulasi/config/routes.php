<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['rekap_pelaporan']['get']            = 'rekapitulasi/rekap_pelaporan';
$route['rekap_pelaporan']['post']           = 'rekapitulasi/rekap_pelaporan/list';
$route['export/rekap_pelaporan']['post']    = 'rekapitulasi/rekap_pelaporan/export';

$route['lapor/add']['get']      			    = 'pelaporan/lapor/add';
$route['lapor/save']['post']    			    = 'pelaporan/lapor/save';
$route['lapor/([0-9]+)/get']['get']             = 'pelaporan/lapor/get/$1';
$route['lapor/([0-9]+)']['get']                 = 'pelaporan/detil/get/$1';
$route['lapor/([0-9]+)']['post']                = 'pelaporan/lapor/update/$1';
$route['lapor/([0-9]+)/del']['get']             = 'pelaporan/lapor/confirm/$1';
$route['lapor/([0-9]+)/del']['post']            = 'pelaporan/lapor/delete/$1';
$route['lapor/([0-9]+)/lampiran']['get']        = 'pelaporan/lapor/confirm_del_lampiran/$1';
$route['lapor/([0-9]+)/lampiran_del']['post']   = 'pelaporan/lapor/delete_lampiran/$1';
