<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Rekap_pelaporan_model extends MY_Model {

    public function list($lembaga_filter = "",  $status_filter = "", $sumber_filter = "", $filter_start = null, $filter_end = null, $filter = NULL, $order_by = 0, $sort = 'DESC', $limit = 0, $ofset = 0){
		$this->db->select("pel.*, sum.*")
					->from($this->t_pelaporan." pel")
					->join("t_pelaporan_lembaga pl", "pel.`int_id_pelaporan` =  pl.`int_id_pelaporan`", "left")
					->join("m_lembaga lem", "pl.`int_id_lembaga` = lem.`int_id_lembaga`", "left")
					->join("m_sumber sum", "pel.`int_id_sumber` = sum.`int_id_sumber`", "left")
					->where("pel.`int_status` != 9 ")
					->group_by("pel.`int_id_pelaporan`");

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_tanggal_pelaporan', $filter_start, $filter_end);
		}
		
		if(($lembaga_filter != "")){ // filter
            $this->db->where('lem.`int_id_lembaga` IN ('.$lembaga_filter.')');
            $this->db->where('pel.`int_status` != 9 ');
		}

		if($sumber_filter!=""){ // filter
            $this->db->where('sum.`int_id_sumber` IN ('.$sumber_filter.')');
		}

		if($status_filter!=""){ // filter
            $this->db->where('pel.`int_status`', ($status_filter));
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_judul_pelaporan', $filter)
					->or_like('txt_detil_pelaporan', $filter)
					->group_end();
		}

		$order = 'dt_tanggal_pelaporan ';
		switch($order_by){
			case 1 : $order = 'dt_tanggal_pelaporan '; break;
			case 2 : $order = 'txt_judul_pelaporan '; break;
			default: {$order = 'dt_tanggal_pelaporan '; $sort = 'DESC';} break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();

	}
	
	public function listCount($lembaga_filter = "",  $status_filter = "", $sumber_filter = "", $filter_start = null, $filter_end = null, $filter = NULL){
		$this->db->select("pel.`int_id_pelaporan`, pel.`txt_judul_pelaporan`, pel.`txt_detil_pelaporan`, pel.`dt_tanggal_pelaporan`, pel.`int_status`, pel.`int_id_sumber`, pel.`txt_url`, lem.`int_id_lembaga`, lem.`txt_nama_lembaga`")
					->from($this->t_pelaporan." pel")
					->join("t_pelaporan_lembaga pl", "pel.`int_id_pelaporan` =  pl.`int_id_pelaporan`", "left")
					->join("m_lembaga lem", "pl.`int_id_lembaga` = lem.`int_id_lembaga`", "left")
					->join("m_sumber sum", "pel.`int_id_sumber` = sum.`int_id_sumber`", "left")
					->where("pel.`int_status` != 9 ")
					->group_by("pel.`int_id_pelaporan`");

		if(!empty($filter_start) && !empty($filter_end)){
			$this->whereBetweenDate('dt_tanggal_pelaporan', $filter_start, $filter_end);
		}
		
		if(($lembaga_filter!="")){ // filter
			$this->db->where('lem.`int_id_lembaga` IN ('.$lembaga_filter.')');
			$this->db->where('pel.`int_status` != 9 ');
		}

		if($sumber_filter!=""){ // filter
			$this->db->where('sum.`int_id_sumber` IN ('.$sumber_filter.')');
		}

		if($status_filter!=""){ // filter
			$this->db->where('pel.`int_status`', ($status_filter));
		}

		if(!empty($filter)){ // filters
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('txt_judul_pelaporan', $filter)
					->or_like('txt_detil_pelaporan', $filter)
					->group_end();
		}
		
		return $this->db->count_all_results();
	}

	public function get_pelaporan_lembaga($int_id_pelaporan){
		return $this->db->query("SELECT lem.`txt_deskripsi` AS `txt_lembaga`
								FROM  {$this->t_pelaporan_lembaga} pl
								LEFT JOIN {$this->m_lembaga} lem ON pl.`int_id_lembaga` = lem.`int_id_lembaga`
								WHERE pl.`int_id_pelaporan` = {$int_id_pelaporan}")->result_array();
	}

	public function get_pelaporan_tanggapan($int_id_pelaporan){
		return $this->db->query("SELECT *
								FROM  {$this->t_tanggapan}
								WHERE `int_id_pelaporan` = {$int_id_pelaporan}")->result_array();
	}

}
