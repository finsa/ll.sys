<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Rekap_pelaporan extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'REKAP-PELAPORAN'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'rekapitulasi';
		$this->routeURL = 'rekap_pelaporan';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('rekap_pelaporan_model', 'model');
		$this->load->model('master/kategori_model', 'kategori');
		$this->load->model('master/sumber_model', 'sumber');
		$this->load->model('master/lembaga_model', 'lembaga');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Rekapitulasi Pelaporan';
		$this->page->menu 	  = 'rekapitulasi';
		$this->page->submenu1 = 'rekap_pelaporan';
		$this->breadcrumb->title = 'Pelaporan';
		$this->breadcrumb->card_title = 'Data Pelaporan';
		$this->breadcrumb->icon = 'fas fa-file-alt';
		$this->breadcrumb->list = ['Rekapitulasi', 'Data Pelaporan'];
		$this->css = true;
		$this->js = true;
		$lembaga_all = '';
		$get_lembaga_user = $this->lembaga->get_list_user();
			foreach($get_lembaga_user as $glu){
				$lembaga_all .=  "'".$glu->int_id_lembaga."',";
			}
		$data['lembaga_all'] = substr($lembaga_all,0,-1);
		if ($data['lembaga_all'] == ''){
			$data['lembaga_list'] = $this->lembaga->get_list();
		}else{
			$data['lembaga_list'] = $this->lembaga->get_list_user();
		}
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('rekap_pelaporan/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');
		$sumber_all = '';
		$get_sumber_user = $this->sumber->get_list_user();
			foreach($get_sumber_user as $gsu){
				$sumber_all .=  "'".$gsu->int_id_sumber."',";
			}
		$sumber = substr($sumber_all,0,-1);

		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';

		$data  = array();
		$total = $this->model->listCount($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), $sumber, $filter_start, $filter_end, $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), $sumber, $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;

			//tujuan/lembaga
			$tujuan_arr = $this->model->get_pelaporan_lembaga($d->int_id_pelaporan);
			$lembaga = '';
			if(!empty($tujuan_arr)){
				$lembaga .= '<ul style="list-style:circle;padding-left:0px;">';
				foreach($tujuan_arr as $tujuan){
					$lembaga .= '<li>'.$tujuan['txt_lembaga'].'</li>';
				}
				$lembaga .= '<ul>';
			}

			//tanggapan
			$tanggapan = $this->get_tanggapan($d->int_id_pelaporan);

			$pelaporan = '<b>'.$d->txt_judul_pelaporan.'</b><br>'.$d->txt_detil_pelaporan;//$judul.$lembaga;
			//No.	Kode	Tanggal	Pelapor	Tujuan	Pelaporan	Tanggapan	Status	Sumber
			$data[] = array($i.'. ', $d->var_kode_pelaporan, idn_date($d->dt_tanggal_pelaporan, 'j F Y'), $d->txt_nama_pelapor, $lembaga, $pelaporan,
							$tanggapan, $d->int_status, $d->txt_sumber.'<br>'.$d->txt_url);
		}
		$this->set_json(array( 'stat' => TRUE,
								'q' => $this->model->getQueryLog(),
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	private function get_tanggapan($int_id_pelaporan){
		$tanggapan_arr = $this->model->get_pelaporan_tanggapan($int_id_pelaporan);
		$tanggapan = '';
		if(!empty($tanggapan_arr)){
			$tanggapan .= '<ul style="list-style:none;padding-left:0px;">';
			foreach($tanggapan_arr as $tang){
				$tanggapan .= '<li><b>'.idn_date($tang['dt_tanggal_tanggapan'], 'j F Y').'</b><br>';
				$tanggapan .= $tang['txt_detil_tanggapan'].'</li>';
			}
			$tanggapan .= '<ul>';
		}
		return $tanggapan;

	}

	public function export(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page
		$sumber_all = '';
		$get_sumber_user = $this->sumber->get_list_user();
			foreach($get_sumber_user as $gsu){
				$sumber_all .=  "'".$gsu->int_id_sumber."',";
			}
		$sumber = substr($sumber_all,0,-1);


		$filter_start = null;
        $filter_end   = null;
        if(strpos($this->input->post('date_filter', true), '~') !== false){
            list($filter_start, $filter_end) = explode(' ~ ', $this->input->post('date_filter', true));
        }

        $filename = 'Rekapitulasi Pelaporan '.$filter_start.'-'.$filter_end.'.xls';
        $title    = 'Rekapitulasi Pelaporan';
        $judul    = "Rekapitulasi Pelaporan";

        $filter_start = convertValidDate($filter_start, 'd-m-Y', 'Y-m-d').' 00:00:00';
        $filter_end	  = convertValidDate($filter_end, 'd-m-Y', 'Y-m-d').' 23:59:59';
		$ldata = $this->model->list($this->input->post('lembaga_filter', true), $this->input->post('status_filter', true), $sumber, $filter_start, $filter_end, $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));
       // $ldata = $this->report->list($filter_start, $filter_end, $this->input->post('kec_id', true), $this->input->post('search_key', TRUE));


        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A2', 'Periode: '.date('d F Y', strtotime($filter_start)).' s/d '.date('d F Y', strtotime($filter_end)).'')
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'Kode')
            ->setCellValue('C4', 'Tanggal')
            ->setCellValue('D4', 'Pelapor')
            ->setCellValue('E4', 'Tujuan')
            ->setCellValue('F4', 'Detil Pelaporan')
            ->setCellValue('G4', 'Tanggapan')
            ->setCellValue('H4', 'Status')
            ->setCellValue('I4', 'Sumber');
	
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('left');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->mergeCells('A3:I3');
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:I4')->getFont()->setBold( true );
		$sheet->getStyle('A4:I4')->getFill()->setFillType('solid')->getStartColor()->setARGB('25B94C');

		
		$sheet->getColumnDimension('A')->setWidth(4); //360
		$sheet->getColumnDimension('B')->setWidth(20); //1350
		$sheet->getColumnDimension('C')->setWidth(15);
		$sheet->getColumnDimension('D')->setWidth(20);
		$sheet->getColumnDimension('E')->setWidth(25);
		$sheet->getColumnDimension('F')->setWidth(40);
		$sheet->getColumnDimension('G')->setWidth(40);
		$sheet->getColumnDimension('H')->setWidth(17);
		$sheet->getColumnDimension('I')->setWidth(30);

		$i = 0;
        $x = 4;
        $total = 0;
		foreach($ldata as $d){
			$i++;
			$x++;

			//pelaporan
			$pelaporan = "[".$d->txt_judul_pelaporan."]\n".$d->txt_detil_pelaporan;

			//tujuan/lembaga
			$tujuan_arr = $this->model->get_pelaporan_lembaga($d->int_id_pelaporan);
			$lembaga = '';
			if(!empty($tujuan_arr)){
				foreach($tujuan_arr as $tujuan){
					$lembaga .= "-".$tujuan['txt_lembaga']."\n";
				}
			}

			//tanggapan
			$tanggapan = $this->get_tanggapan_excel($d->int_id_pelaporan);

			//status
			switch ($d->int_status) {
				case 0:
					$status = 'Belum Ditanggapi';
					break;
				case 1:
					$status = 'Dalam Penanganan';
					break;
				case 2:
					$status = 'Dalam Perencanaan';
					break;
				case 3:
					$status = 'Selesai';
					break;
				default:
					$status = 'Belum Dimoderasi';
					break;
			}

			//No.	Kode	Tanggal	Pelapor	Tujuan	Pelaporan	Tanggapan	Status	Sumber
			$sheet->setCellValue('A'.$x, $i);
			$sheet->setCellValue('B'.$x, $d->var_kode_pelaporan);
			$sheet->setCellValue('C'.$x, idn_date($d->dt_tanggal_pelaporan, 'j F Y'));
			$sheet->getStyle('C'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('C'.$x)->getAlignment()->setHorizontal('right');
			$sheet->setCellValue('D'.$x, $d->txt_nama_pelapor);
			$sheet->getStyle('D'.$x)->getAlignment()->setWrapText(true);
			$sheet->setCellValue('E'.$x, $lembaga);
			$sheet->getStyle('E'.$x)->getAlignment()->setWrapText(true);
			$sheet->setCellValue('F'.$x, trim(str_replace("&nbsp;", "", strip_tags($pelaporan))));
			$sheet->getStyle('F'.$x)->getAlignment()->setWrapText(true);
			$sheet->setCellValue('G'.$x, $tanggapan);
			$sheet->getStyle('G'.$x)->getAlignment()->setWrapText(true);
			$sheet->setCellValue('H'.$x, $status);
			$sheet->setCellValue('I'.$x, $d->txt_sumber."\n".$d->txt_url);
			$sheet->getStyle('I'.$x)->getAlignment()->setWrapText(true);
			$sheet->getStyle('A1:I'.$x)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP);

		}

        /*foreach(range('A','I') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }*/

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
		$writer->save('php://output');
		exit;
	}
	
	private function get_tanggapan_excel($int_id_pelaporan){
		$tanggapan_arr = $this->model->get_pelaporan_tanggapan($int_id_pelaporan);
		$tanggapan = '';
		if(!empty($tanggapan_arr)){
			foreach($tanggapan_arr as $tang){
				$tanggapan .= "[".idn_date($tang['dt_tanggal_tanggapan'], 'j F Y')."]\n";
				$tanggapan .= str_replace("&nbsp;", "", strip_tags($tang['txt_detil_tanggapan']))."\n";
			}
		}
		return $tanggapan;

	}
}
